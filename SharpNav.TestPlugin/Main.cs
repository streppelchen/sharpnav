﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using Serilog;
using Serilog.Core;
using SharpNav;
using SharpNav.Pathfinding;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TestPlugin
{
    public class Main : AOPluginEntry
    {
        private DungeonNavMeshFactory _dungeonNavMeshFactory;
        public Logger Log;

        // private SPath _borPath;

        public override void Run()
        {
            // _borPath = SPath.Create(false);

            //_borPath.AddPoints(new List<Vector3>
            //{
            //    new Vector3(694.9, 66.8, 676.9),
            //    new Vector3(687.2, 66.8, 679.7),
            //    new Vector3(681.4, 66.8, 674.9)
            //});

            //Chat.RegisterCommand("moveinbor", (string command, string[] param, ChatWindow chatWindow) =>
            //{
            //    SMovementController.SetPath(_borPath);
            //});


            _dungeonNavMeshFactory = new DungeonNavMeshFactory();

            SMovementController.Set();
            // PATHING TEST CODE

            Log = Logger;

            Log.Information("SharpNav TestPlugin loaded");

            var testPathDir = $"{PluginDirectory}\\TestPath";

            if (Playfield.ModelIdentity.Instance == (int)PlayfieldId.Grid)
            {
                SPath gridPath1 = SPath.Load($"{testPathDir}\\GridTest1.json");
                SPath gridPath2 = SPath.Load($"{testPathDir}\\GridTest2.json");
                SPath gridPath3 = SPath.Load($"{testPathDir}\\GridTest3.json");
                SPath gridPath4 = SPath.Load($"{testPathDir}\\GridTest4.json");
               
                SLink link1 = new SLink(gridPath1, gridPath2, SPathLinkType.TeleportPad);
                SLink link2 = new SLink(gridPath2, gridPath3, SPathLinkType.TeleportPad);
                SLink link3 = new SLink(gridPath3, gridPath4, SPathLinkType.TeleportPad);
               
                SPathManager.CreateRoute("GridTest", new List<SLink> { link1, link2, link3 });
            }



            Chat.RegisterCommand("testlink", (string command, string[] param, ChatWindow chatWindow) =>
            {
                SPathManager.SetActiveRoute(SPathManager.Routes.FirstOrDefault(x => x.Name == "GridTest"), true);
            });

            Chat.RegisterCommand("stopmoving", (string command, string[] param, ChatWindow chatWindow) =>
            {
                SMovementController.Halt();
            });

            Chat.RegisterCommand("startmoving", (string command, string[] param, ChatWindow chatWindow) =>
            {
                SPathManager.TraverseActiveRoute();
            });

            //SMovementControllerSettings mSettings = new SMovementControllerSettings
            //{
            //    NavMeshSettings = SNavMeshSettings.Default,
            //    PathSettings = SPathSettings.Default,
            //};

            // SMovementControllerSettings.Set(mSettings); // or simply use SMovementController.Set() if the default values are used

            string navFilePath = $"C:\\navmeshes";

             SMovementController.AutoLoadNavmeshes(navFilePath); // automatically loads meshes if file exists
            //SMovementController.AutoBakeNavmeshes(navFilePath);
            //SMovementController.AutoLoadNavmeshes($"C:\\navmeshes", (playfield, isDungeon) => isDungeon || playfield != (int)PlayfieldId.Andromeda); // automatically loads or bakes meshes if meets condition

            Chat.RegisterCommand("saveobj", (string command, string[] param, ChatWindow chatWindow) =>
            {
                ObjParser.SaveCurrentPlayfield($"C:\\navmeshes", true);
            });

            Chat.RegisterCommand("genobj", (string command, string[] param, ChatWindow chatWindow) =>
            {
                var objPath = $"C:\\navmeshes\\{Playfield.ModelIdentity.Instance}.obj";
                var navPath = objPath.ChangePathExtension(".nav");

                if (SNavMeshGenerator.GenerateFromObj(NavMeshGenerationSettings.LowDensity, objPath, out NavMesh navMesh))
                    SNavMeshSerializer.SaveToFile(navMesh, navPath);
            });

            Chat.RegisterCommand("borgridlink", (string command, string[] param, ChatWindow chatWindow) =>
            {
                SMovementController.RegisterPfLink(ZoneId.Borealis, new SPlayfieldLink(Terminal.BorGrid, "Enter The Grid", LinkType.Portal, ZoneId.Grid, true));
                SMovementController.SetNavDestination(ZoneId.Grid);
            });

            Chat.RegisterCommand("gennavmesh", (string command, string[] param, ChatWindow chatWindow) =>
            {
                NavMeshGenerationSettings customDensity = NavMeshGenerationSettings.CustomDensity(0.5f);

                SNavMeshGenerator.GenerateAsync(customDensity).ContinueWith(navMesh =>
                {
                    if (navMesh.Result == null)
                        return;

                    SMovementController.LoadNavmesh(navMesh.Result);
                    SNavMeshSerializer.SaveToFile(navMesh.Result, $"C:\\navmeshes\\{Playfield.ModelIdentity.Instance}.nav");
                });
            });

            Chat.RegisterCommand("dumpdungeon", (string command, string[] param, ChatWindow chatWindow) =>
            {
                if (!Playfield.IsDungeon)
                {
                    Log.Information("This playfield is not a dungeon.");
                    return;
                }

                Log.Information("Rooms:");

                foreach (Room room in Playfield.Rooms)
                {
                    Log.Information($"\t{room.Instance} - {room.Name} @ {room.Pointer.ToString("X")} Rot: {room.Rotation}");

                    //Log.Information("\tDoors:");

                    //for(int i = 0; i < room.NumDoors; i++)
                    //{
                    //    room.GetDoorPosRot(i, out Vector3 pos, out Quaternion rot);
                    //    Log.Information($"\t\t{i}. {pos} / {rot.Yaw}");
                    //}
                }
            });

            //Alternative dungeon generation
            Chat.RegisterCommand("gendungeon", (string command, string[] param, ChatWindow chatWindow) =>
            {
                SNavMeshGenerator.GenerateDungeonAsync(NavMeshGenerationSettings.HighDensity, true).ContinueWith(navMesh =>
                {
                    if (navMesh.Result == null)
                        return;

                    SMovementController.LoadNavmesh(navMesh.Result);
                    SNavMeshSerializer.SaveToFile(navMesh.Result, $"C:\\navmeshes\\{Playfield.ModelIdentity.Instance}.nav");
                });
            });

            Chat.RegisterCommand("gendungeonasync", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _dungeonNavMeshFactory.GenerateNavMeshAsync().ContinueWith(navMesh =>
                {
                    if (navMesh.Result == null)
                    {
                        Log.Information($"Failed.");
                        return;
                    }

                    SMovementController.LoadNavmesh(navMesh.Result);
                    Log.Information($"Dungeon NavMesh Ready.");
                });
            });

            Chat.RegisterCommand("gendungeon2", (string command, string[] param, ChatWindow chatWindow) =>
            {
                try
                {
                    NavMesh navMesh = _dungeonNavMeshFactory.GenerateNavMesh();
                    SMovementController.LoadNavmesh(navMesh);
                    Log.Information($"Dungeon NavMesh Ready.");
                }
                catch (Exception e)
                {
                    Log.Information(e.Message);
                }
            });

            Chat.RegisterCommand("loadnavmesh", (string command, string[] param, ChatWindow chatWindow) =>
            {
                if (SNavMeshSerializer.LoadFromFile($"C:\\navmeshes\\{Playfield.ModelIdentity.Instance}.nav", out NavMesh navMesh))
                {
                    SMovementController.LoadNavmesh(navMesh);
                }
            });

            Chat.RegisterCommand("autoloadnavmesh", (string command, string[] param, ChatWindow chatWindow) =>
            {
                SMovementController.AutoLoadNavmeshes($"C:\\navmeshes");
            });

            Chat.RegisterCommand("clearnavmesh", (string command, string[] param, ChatWindow chatWindow) =>
            {
                SMovementController.UnloadNavmesh();
            });

            Chat.RegisterCommand("togglenavmeshdraw", (string command, string[] param, ChatWindow chatWindow) =>
            {
                SMovementController.ToggleNavMeshDraw();
            });

            Chat.RegisterCommand("togglepathdraw", (string command, string[] param, ChatWindow chatWindow) =>
            {
                SMovementController.TogglePathDraw();
            });

            Chat.RegisterCommand("setpath", (string command, string[] param, ChatWindow chatWindow) =>
            {
                try
                {
                    SMovementController.SetNavDestination(new Vector3(168, 5.0f, 140f));
                }
                catch (Exception e)
                {
                    Log.Information(e.Message);
                }
            //Vector3 endPos = new Vector3(558.7f, 353.7f, 594.4f); //xan hub inf ring

            //bor
            //Vector3 endPos = new Vector3(637.8f, 68.0f, 468.7f); // bor subway
            //Vector3 endPos = new Vector3(636.9f, 66.8f, 726.3f); // bor grid

            //db2
            //Vector3 endPos = new Vector3( 301.0f, 135.3f,163.0f); //db2 entrance
            //Vector3 endPos = new Vector3(285.4f, 133.3f, 233.1f); //db2  center rock

            //subway
            //Vector3 endPos = new Vector3(332.4f, 73.6f, 99.0f); //abmouth lair
            //Vector3 endPos = new Vector3(116.9f, 77.0f, 127.0f); // ranom upstairs
            //Vector3 endPos = new Vector3(3449.2f, 0f, 889.1f); // arete credit card
            //Vector3 endPos = new Vector3(3368.924, 18.11222, 828.3152); // arete 'Vaughn Hammond'
            //Vector3 endPos = new Vector3(3362.93, 17.82114, 834.937); // arete 'Enter ICC HQ'
            //Vector3 endPos = new Vector3(3595.875, 51.95052, 785.9075); // arete 'Shuttle Door'
            //SMovementController.SetDestination(endPos);
            });

            Chat.RegisterCommand("setpath2", (string command, string[] param, ChatWindow chatWindow) =>
            {
                try
                { 
                    SMovementController.SetNavDestination(new Vector3(1600, 52.0f, 205f));
                }
                catch (Exception e)
                {
                    Log.Information(e.Message);
                }
            });

            Chat.RegisterCommand("pointtest", (string command, string[] param, ChatWindow chatWindow) =>
            {
                try
                {
                    if (SMovementController.NavAgent.FindNearestNavPoint(DynelManager.LocalPlayer.Position, new Vector3(3, 3, 3), out NavPoint point))
                    {
                        Log.Information($"NavPoint: {point.Polygon.Id} @ {point.Position}");
                        SMovementController.NavAgent.NavMesh.TryGetTileAndPolyByRefUnsafe(point.Polygon, out NavTile tile, out NavPoly poly);

                        Log.Information($"Tile: {SMovementController.NavAgent.NavMesh.Tiles.IndexOf(tile)} Poly: {Array.IndexOf(tile.Polys, poly)}");
                        Log.Information($"{SMovementController.NavAgent.NavMesh.IdManager.Encode(1, 12, 0)}");
                    }
                }
                catch (Exception e)
                {
                    Log.Information(e.Message);
                }
            });

            Chat.RegisterCommand("appendpath", (string command, string[] param, ChatWindow chatWindow) =>
            {
                Vector3 pos1 = new Vector3(636.4, 66.8, 728.8);
                Vector3 pos2 = new Vector3(651.8, 72.8, 549.6);
                SMovementController.AppendNavDestination(pos1); //localplayer position to pos1
                SMovementController.AppendNavDestination(pos1, new Vector3(651.8, 72.8, 549.6)); //pos1 to pos2
            });

            Game.OnUpdate += OnUpdate;
            SMovementController.DestinationReached += OnDestinationReached;
            SMovementController.LinkDestinationReached += OnLinkDestinationReached;
            SMovementController.OnRubberband += OnRubberband;
            SMovementController.Stuck += OnStuck;
            SMovementController.AgentStateChange += OnAgentStateChange;
            SNavMeshGenerator.BakeStatus += OnBakeStatus;

            //Overrides the default on stuck method (which teleports the player forward slightly) with user given input
            SMovementController.SetStuckLogic(() =>
            {
            });
        }

        private void OnRubberband(Vector3 pos)
        {
            Log.Information($"I got rubberbanded to position: {pos}");

            Vector3 searchRange = new Vector3(1, 1, 1) * 100;

            if (!SMovementController.GetClosestNavPoint(searchRange, out Vector3 navPoint))
            {
                Log.Information("Couldn't find nearby point");
                return;
            }

            Log.Information($"Found nearby point at coordinate: {navPoint}");
        }

        private void OnBakeStatus(BakeState state)
        {
            Log.Information($"Bake state: {state}");
        }

        private void OnLinkDestinationReached(SNavLink link)
        {
            Log.Information($"I have reached a NavLink destination point: {link.Name} {link.Position}");
        }

        private void OnAgentStateChange(NavMeshQuery.AgentNavMeshState agentState)
        {
            Log.Information($"Agent state changed to '{agentState}'");
        }

        private void OnDestinationReached(Vector3 pos)
        {
            Log.Information($"Destination point reached: {pos}!");
        }

        private void OnStuck(Vector3 stuckPos, Vector3 destPos)
        {
            if (SMovementController.IsNavigating())
                SMovementController.SetNavDestination(destPos);

            Log.Information($"I'm stuck at position: {stuckPos}! My final destination position is: {destPos}");
        }

        private void OnUpdate(object sender, float e)
        {
            //SMovementController.SetNavDestination(SMovementController.GetRandomPoint(), PathingType.Interpolated, true);

            //foreach (Room room in Playfield.Rooms)
            //{
            //    //if (room.Instance != 6)
            //    //    continue;

            //    for (int i = 0; i < room.NumDoors; i++)
            //    {
            //        if (room.GetDoorConnectZone(i) == room.Instance)
            //            continue;

            //        room.GetDoorPosRot(i, out Vector3 pos, out Quaternion rot);

            //        Vector3 Pos0 = pos + rot.Forward;
            //        Vector3 Pos1 = pos;
            //        Debug.DrawLine(Pos0, Pos1, DebuggingColor.Purple);
            //        Debug.DrawSphere(Pos0, 0.2f, DebuggingColor.Purple);
            //        //Debug.DrawSphere(Pos1, 0.2f, DebuggingColor.Purple);
            //    }
            //}
        }
    }
}