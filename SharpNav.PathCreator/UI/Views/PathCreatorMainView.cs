﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;

namespace SharpNav.PathCreator
{
    public class PathCreatorMainView : PathCreatorView
    {
        private TextView _pointsText;
        private TextView _isLoopingText;
        private TextView _isReversedText;
        private TextView _isLockedText;
        private TextInputView _pathName;

        public PathCreatorMainView(string path, PathCreatorWindow window) : base(path, window)
        {

            try
            {
                if (Root.FindChild("AddPoint", out Button addPoint)) { addPoint.Clicked = AddPointClick; }
                if (Root.FindChild("RemovePoint", out Button removePoint)) { removePoint.Clicked = RemovePointClick; }
                if (Root.FindChild("ReversePath", out Button reversePath)) { reversePath.Clicked = ReversePathClick; }
                if (Root.FindChild("RunPath", out Button runPath)) { runPath.Clicked = RunPathClicked; }
                if (Root.FindChild("PickupPoint", out Button pickupPoint)) { pickupPoint.Clicked = PickupPointClick; }
                if (Root.FindChild("PlacePoint", out Button placePoint)) { placePoint.Clicked = PlacePointClick; }
                if (Root.FindChild("SelectPoint", out Button selectPoint)) { selectPoint.Clicked = SelectPointClick; }
                if (Root.FindChild("UnselectPoint", out Button unselectPoint)) { unselectPoint.Clicked = UnselectPointClick; }
                if (Root.FindChild("SplitPath", out Button splitPath)) { splitPath.Clicked = SplitPointClick; }
                if (Root.FindChild("PathName", out _pathName)) { }
                if (Root.FindChild("ExportPath", out Button exportPath)) { exportPath.Clicked = ExportPathClick; }
                if (Root.FindChild("ToggleLock", out Button toggleLock)) { toggleLock.Clicked = ToggleLockClick; }
                if (Root.FindChild("ToggleLoop", out Button toggleLoop)) { toggleLoop.Clicked = ToggleLoopClick; }
                if (Root.FindChild("ClearPath", out Button clearPath)) { clearPath.Clicked = ClearClick; }
                if (Root.FindChild("Points", out _pointsText)) { _pointsText.Text = Parent.SPath.Waypoints.Count.ToString(); }
                if (Root.FindChild("IsLooping", out _isLoopingText)) { _isLoopingText.Text = Parent.SPath.IsLooping.ToString(); }
                if (Root.FindChild("IsReversed", out _isReversedText)) { _isReversedText.Text = Parent.SPath.IsReversed.ToString(); }
                if (Root.FindChild("IsLocked", out _isLockedText)) { _isLockedText.Text = Parent.SPath.IsLocked.ToString(); }
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message);
            }
        }
        private void AddPointClick(object sender, ButtonBase e)
        {
            Parent.SPath.AddPoint();
            _pointsText.Text = Parent.SPath.Waypoints.Count.ToString();
        }

        private void RemovePointClick(object sender, ButtonBase e)
        {
            Parent.SPath.RemovePoint(); 
            _pointsText.Text = Parent.SPath.Waypoints.Count.ToString();
        }

        private void PickupPointClick(object sender, ButtonBase e)
        {
            Parent.SPath.PickupPoint();
        }

        private void PlacePointClick(object sender, ButtonBase e)
        {
            Parent.SPath.PlacePoint();
        }

        private void SelectPointClick(object sender, ButtonBase e)
        {
            Parent.SPath.SelectPoint();
        }

        private void UnselectPointClick(object sender, ButtonBase e)
        {
            Parent.SPath.UnselectPoint();
        }

        private void RunPathClicked(object sender, ButtonBase e)
        {
            if (Parent.SPath == null)
                return;

            if (!SMovementController.IsLoaded())
                SMovementController.Set();

            SMovementController.SetPath(Parent.SPath, true);
        }

        private void ExportPathClick(object sender, ButtonBase e)
        {
            if (string.IsNullOrEmpty(_pathName.Text))
            {
                Chat.WriteLine("Please specify a path name!");
                return;
            }

            var path = $"{Main.PluginDir}\\{_pathName.Text}.json";
            Parent.SPath.Name = _pathName.Text;
            Parent.SPath.Export(path);

            Chat.WriteLine($"Exported at: {path}", ChatColor.Green);
        }

        private void SplitPointClick(object sender, ButtonBase e)
        {
            if (!Parent.SPath.Split())
                return;

            _pointsText.Text = Parent.SPath.Waypoints.Count.ToString();
        }

        private void ToggleLockClick(object sender, ButtonBase e)
        {
            Parent.SPath.ToggleLock();
            _isLockedText.Text = Parent.SPath.IsLocked.ToString();
        }

        private void ToggleLoopClick(object sender, ButtonBase e)
        {
            Parent.SPath.ToggleLoop();
            _isLoopingText.Text = Parent.SPath.IsLooping.ToString();
        }

        private void ClearClick(object sender, ButtonBase e)
        {
            Parent.SPath.Clear();
            Parent.SPath.IsLocked = false;
        }

        private void ReversePathClick(object sender, ButtonBase e)
        {
            Parent.SPath.Reverse();
            _isReversedText.Text = Parent.SPath.IsReversed.ToString();
        }
    }
}