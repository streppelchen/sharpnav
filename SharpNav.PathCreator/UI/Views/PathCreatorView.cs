﻿using AOSharp.Common.GameData.UI;
using AOSharp.Core.UI;
using SharpNav.Crowds;
using System;

namespace SharpNav.PathCreator
{
    public class PathCreatorView
    {
        protected View Root;
        public PathCreatorWindow Parent;

        public PathCreatorView(string viewPath, PathCreatorWindow parent)
        {
            Root = View.CreateFromXml(viewPath);
            Parent = parent;
            Parent.AddChild(Root);
        }

        public void Dispose()
        {
            Parent.RemoveChild(Root);   
        }
    }
}