﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Pathfinding;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace SharpNav.ConsoleApp
{
    public class NavMeshGenConfig : NavMeshGenerationSettings
    {
        public string ObjPath;

        public static NavMeshGenConfig Load()
        {
            try
            {
                var path = "Config.json";

                return !File.Exists(path) ? LoadDefaults(path) : JsonConvert.DeserializeObject<NavMeshGenConfig>(File.ReadAllText(path));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private static NavMeshGenConfig LoadDefaults(string path)
        {
            NavMeshGenConfig serializer = new NavMeshGenConfig();

            serializer.CellSize = HighDensity.CellSize;
            serializer.CellHeight = HighDensity.CellHeight;
            serializer.MaxClimb = HighDensity.MaxClimb;
            serializer.AgentHeight = HighDensity.AgentHeight;
            serializer.AgentRadius = HighDensity.AgentRadius;
            serializer.MinRegionSize = HighDensity.MinRegionSize;
            serializer.MergedRegionSize = HighDensity.MergedRegionSize;
            serializer.MaxEdgeLength = HighDensity.MaxEdgeLength;
            serializer.MaxEdgeError = HighDensity.MaxEdgeError;
            serializer.VertsPerPoly = HighDensity.VertsPerPoly;
            serializer.SampleDistance = HighDensity.SampleDistance;
            serializer.MaxSampleError = HighDensity.MaxSampleError;
            serializer.Bounds = HighDensity.Bounds;
            serializer.ContourFlags = HighDensity.ContourFlags;
            serializer.BuildBoundingVolumeTree = HighDensity.BuildBoundingVolumeTree;
            serializer.FilterLargestSection = HighDensity.FilterLargestSection;
            serializer.ObjPath = $"C:\\navmeshes\\800.obj";

            File.WriteAllText(path, JsonConvert.SerializeObject(serializer, Formatting.Indented));

            return serializer;
        }
    }
}