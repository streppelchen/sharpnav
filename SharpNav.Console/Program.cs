﻿using AOSharp.Pathfinding;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SharpNav.ConsoleApp
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            try
            {
                NavMeshGenConfig genSettings = NavMeshGenConfig.Load();
                var objPath = genSettings.ObjPath;
                var navPath = objPath.ChangePathExtension(".nav");

                var loader = new Loader(genSettings.ObjPath);
                loader.Start();

                await Task.Run(() =>
                {
                    if (SNavMeshGenerator.GenerateFromObj(genSettings, objPath, out NavMesh navMesh))
                        SNavMeshSerializer.SaveToFile(navMesh, navPath);
                });

                loader.Stop(out TimeSpan elapsedTime);

                Console.Clear();
                Console.WriteLine($"Total time: {elapsedTime.ToString(@"hh\:mm\:ss")}\nNavmesh file saved at '{navPath}'");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Console.ReadLine();
        }
    }
}