﻿using System.Collections.Generic;
using System.IO;
using SharpNav.Collections;
using SharpNav.Geometry;
using SharpNav.Pathfinding;
using AOSharp.Common.GameData;
using System.Linq;
using AOSharp.Core.UI;
using static SharpNav.PolyMeshDetail;
using Vector3 = SharpNav.Geometry.Vector3;
using AOSharp.Core;
using AOSharp.Pathfinding;

namespace SharpNav.IO.Json
{
    /// <summary>
    /// Subclass of NavMeshSerializer that implements 
    /// serialization/deserializtion in text files with json format
    /// </summary>
    public class NavMeshSerializer
    {
        private static readonly string NAVMESH_VERSION = "1.0.0";

        public NavMeshSerializer()
        {
        }

        public void Serialize(string path, NavMesh mesh)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.Create)))
            {
                writer.Write(NAVMESH_VERSION);
                WriteNavMeshGenerationSettings(writer, mesh.Settings);
                WriteVector3(writer, mesh.Origin);
                writer.Write(mesh.TileWidth);
                writer.Write(mesh.TileHeight);
                writer.Write(mesh.MaxTiles);
                writer.Write(mesh.MaxPolys);

                writer.Write(mesh.Tiles.Count());

                foreach (NavTile tile in mesh.Tiles)
                {
                    NavPolyId id = mesh.GetTileRef(tile);
                    WriteNavTile(writer, tile, id);
                }
            }
        }

        public void Serialize(string path, NavMeshBuilder builder)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.Create)))
            {
                WriteNavBuilder(writer, builder);
            }
        }

        public void Serialize(string path, PolyMesh polyMesh, PolyMeshDetail detailMesh, NavMeshGenerationSettings settings)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.Create)))
            {
                WriteNavMeshGenerationSettings(writer, settings);
                WritePolyMesh(writer, polyMesh);
                WritePolyMeshDetail(writer, detailMesh);
            }
        }

        private void WritePolyMesh(BinaryWriter writer, PolyMesh polyMesh)
        {
            writer.Write(polyMesh.Verts.Length);

            foreach (PolyVertex vert in polyMesh.Verts)
                WritePolyVertex(writer, vert);

            writer.Write(polyMesh.Polys.Length);

            foreach (PolyMesh.Polygon poly in polyMesh.Polys)
                WritePolygon(writer, poly);

            WriteBounds(writer, polyMesh.Bounds);
            writer.Write(polyMesh.NumVertsPerPoly);
            writer.Write(polyMesh.CellSize);
            writer.Write(polyMesh.CellHeight);
            writer.Write(polyMesh.BorderSize);
        }

        private void WritePolyMeshDetail(BinaryWriter writer, PolyMeshDetail detailMesh)
        {
            writer.Write(detailMesh.Meshes.Length);

            foreach (var mesh in detailMesh.Meshes)
                WriteMeshData(writer, mesh);

            writer.Write(detailMesh.Verts.Length);

            foreach (Vector3 vert in detailMesh.Verts)
                WriteVector3(writer, vert);

            writer.Write(detailMesh.Tris.Length);

            foreach (var tri in detailMesh.Tris)
                WriteTriangleData(writer, tri);
        }

        private void WriteNavMeshGenerationSettings(BinaryWriter writer, NavMeshGenerationSettings settings)
        {
            writer.Write(settings.CellSize);
            writer.Write(settings.CellHeight);
            writer.Write(settings.MaxClimb);
            writer.Write(settings.AgentHeight);
            writer.Write(settings.AgentRadius);
            writer.Write(settings.MinRegionSize);
            writer.Write(settings.MergedRegionSize);
            writer.Write(settings.MaxEdgeLength);
            writer.Write(settings.MaxEdgeError);
            writer.Write((byte)settings.ContourFlags);
            writer.Write(settings.VertsPerPoly);
            writer.Write(settings.SampleDistance);
            writer.Write(settings.MaxSampleError);
            writer.Write(settings.BuildBoundingVolumeTree);
            writer.Write(settings.Bounds.MinX);
            writer.Write(settings.Bounds.MinY);
            writer.Write(settings.Bounds.MaxX);
            writer.Write(settings.Bounds.MaxY);
        }

        private void WritePolygon(BinaryWriter writer, PolyMesh.Polygon polygon)
        {
            writer.Write(polygon.Vertices.Length);

            foreach (int vert in polygon.Vertices)
                writer.Write(vert);

            writer.Write(polygon.NeighborEdges.Length);

            foreach (int edge in polygon.NeighborEdges)
                writer.Write(edge);

            writer.Write(polygon.Area.Id);
            writer.Write(polygon.RegionId.Id);
        }

        private void WriteVector3(BinaryWriter writer, Geometry.Vector3 vector)
        {
            writer.Write(vector.X);
            writer.Write(vector.Y);
            writer.Write(vector.Z);
        }

        private void WritePolyVertex(BinaryWriter writer, PolyVertex polyVertex)
        {
            writer.Write(polyVertex.X);
            writer.Write(polyVertex.Y);
            writer.Write(polyVertex.Z);
        }

        private void WriteNavMeshInfo(BinaryWriter writer, PathfindingCommon.NavMeshInfo header)
        {
            writer.Write(header.X);
            writer.Write(header.Y);
            writer.Write(header.Layer);
            writer.Write(header.PolyCount);
            writer.Write(header.VertCount);
            writer.Write(header.MaxLinkCount);
            writer.Write(header.DetailMeshCount);
            writer.Write(header.DetailVertCount);
            writer.Write(header.DetailTriCount);
            writer.Write(header.BvNodeCount);
            writer.Write(header.OffMeshConCount);
            writer.Write(header.OffMeshBase);
            writer.Write(header.WalkableHeight);
            writer.Write(header.WalkableRadius);
            writer.Write(header.WalkableClimb);
            WriteBounds(writer, header.Bounds);
            writer.Write(header.BvQuantFactor);
        }

        private void WriteNavBuilder(BinaryWriter writer, NavMeshBuilder builder)
        {
            WriteNavMeshInfo(writer, builder.Header);

            writer.Write(builder.NavPolys.Length);

            foreach (NavPoly navPoly in builder.NavPolys)
                WriteNavPoly(writer, navPoly);

            writer.Write(builder.NavVerts.Length);

            foreach (Geometry.Vector3 vector in builder.NavVerts)
                WriteVector3(writer, vector);

            writer.Write(builder.NavDMeshes.Length);

            foreach (MeshData meshData in builder.NavDMeshes)
                WriteMeshData(writer, meshData);

            writer.Write(builder.NavDVerts.Length);

            foreach (Geometry.Vector3 detailVert in builder.NavDVerts)
                WriteVector3(writer, detailVert);

            writer.Write(builder.NavDTris.Length);

            foreach (TriangleData triData in builder.NavDTris)
                WriteTriangleData(writer, triData);

            writer.Write(builder.OffMeshCons.Length);

            foreach (OffMeshConnection meshConnection in builder.OffMeshCons)
                WriteOffMeshConnection(writer, meshConnection);

            writer.Write(builder.NavBvTree.Count);

            for (int i = 0; i < builder.NavBvTree.Count; i++)
                WriteBVTreeNode(writer, builder.NavBvTree[i]);
        }

        private void WriteNavTile(BinaryWriter writer, NavTile tile, NavPolyId id)
        {
            writer.Write(id.Id);
            WriteVector2i(writer, tile.Location);
            writer.Write(tile.Layer);
            writer.Write(tile.Salt);
            WriteBounds(writer, tile.Bounds);

            writer.Write(tile.Polys.Count());

            foreach (NavPoly navPoly in tile.Polys)
                WriteNavPoly(writer, navPoly);

            writer.Write(tile.Verts.Count());

            foreach (Geometry.Vector3 vector in tile.Verts)
                WriteVector3(writer, vector);

            writer.Write(tile.DetailMeshes.Count());

            foreach (MeshData meshData in tile.DetailMeshes)
                WriteMeshData(writer, meshData);

            writer.Write(tile.DetailVerts.Count());

            foreach (Geometry.Vector3 detailVert in tile.DetailVerts)
                WriteVector3(writer, detailVert);

            writer.Write(tile.DetailTris.Count());

            foreach (TriangleData triData in tile.DetailTris)
                WriteTriangleData(writer, triData);

            writer.Write(tile.OffMeshConnections.Count());

            foreach (OffMeshConnection meshConnection in tile.OffMeshConnections)
                WriteOffMeshConnection(writer, meshConnection);

            writer.Write(tile.BVTree.Count);

            for (int i = 0; i < tile.BVTree.Count; i++)
                WriteBVTreeNode(writer, tile.BVTree[i]);

            writer.Write(tile.BvQuantFactor);
            writer.Write(tile.BvNodeCount);
            writer.Write(tile.WalkableClimb);
        }

        private void WriteTriangleData(BinaryWriter writer, TriangleData triData)
        {
            writer.Write(triData.VertexHash0);
            writer.Write(triData.VertexHash1);
            writer.Write(triData.VertexHash2);
            writer.Write(triData.Flags);
        }

        private void WriteMeshData(BinaryWriter writer, MeshData meshData)
        {
            writer.Write(meshData.VertexIndex);
            writer.Write(meshData.VertexCount);
            writer.Write(meshData.TriangleIndex);
            writer.Write(meshData.TriangleCount);
        }

        private void WriteBounds(BinaryWriter writer, BBox3 bounds)
        {
            WriteVector3(writer, bounds.Min);
            WriteVector3(writer, bounds.Max);
        }

        private void WriteVector2i(BinaryWriter writer, Vector2i location)
        {
            writer.Write(location.X);
            writer.Write(location.Y);
        }

        private void WriteBVTreeNode(BinaryWriter writer, BVTree.Node node)
        {
            WritePolyVertex(writer, node.Bounds.Min);
            WritePolyVertex(writer, node.Bounds.Max);
            writer.Write(node.Index);
        }

        private void WriteOffMeshConnection(BinaryWriter writer, OffMeshConnection meshConnection)
        {
            WriteVector3(writer, meshConnection.Pos0);
            WriteVector3(writer, meshConnection.Pos1);
            writer.Write(meshConnection.Radius);
            writer.Write(meshConnection.Poly);
            writer.Write((byte)meshConnection.Flags);
            writer.Write((byte)meshConnection.Side);
            // writer.Write(meshConnection.Tag); not sure what this is, skipping for now
        }

        private void WriteNavPoly(BinaryWriter writer, NavPoly navPoly)
        {
            writer.Write((byte)navPoly.PolyType);

            writer.Write(navPoly.Links.Count());

            foreach (Link link in navPoly.Links)
                WriteLink(writer, link);

            writer.Write(navPoly.Verts.Count());

            foreach (var vert in navPoly.Verts)
                writer.Write(vert);

            writer.Write(navPoly.Neis.Count());

            foreach (var neis in navPoly.Neis)
                writer.Write(neis);

            // writer.Write(navPoly.Tag); Not sure what this is, skipping for now
            writer.Write(navPoly.VertCount);
            writer.Write(navPoly.Area.Id);
        }

        private void WriteLink(BinaryWriter writer, Link link)
        {
            writer.Write(link.Reference.Id);
            writer.Write(link.Edge);
            writer.Write((byte)link.Side);
            writer.Write(link.BMin);
            writer.Write(link.BMax);
        }

        public RoomTile DeserializeRoomTile(string path)
        {
            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    return new RoomTile
                    {
                        Settings = ReadNavMeshGenerationSettings(reader),
                        PolyMesh = ReadPolyMesh(reader),
                        DetailMesh = ReadPolyMeshDetail(reader)
                    };
                }
            }
        }

        public NavMeshBuilder DeserializeBuilder(string path)
        {
            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    return ReadNavMeshBuilder(reader);
                }
            }
        }

        public NavMesh Deserialize(string path)
        {
            if (System.IO.Path.GetExtension(path) != ".nav")
            {
                Chat.WriteLine("Invalid file extension.", ChatColor.Red);
                return null;
            }

            NavMesh navMesh;

            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    string version = reader.ReadString();

                    if (version != NAVMESH_VERSION)
                    {
                        Chat.WriteLine("File is using an old nav mesh version. Loading is cancelled.", ChatColor.Red);
                        return null;
                    }

                    NavMeshGenerationSettings settings = ReadNavMeshGenerationSettings(reader);

                    navMesh = new NavMesh(ReadVector3(reader), reader.ReadSingle(), reader.ReadSingle(), reader.ReadInt32(), reader.ReadInt32());
                    navMesh.Settings = settings;

                    List<NavTile> tiles = new List<NavTile>();

                    int tileCount = reader.ReadInt32();

                    for (int i = 0; i < tileCount; i++)
                    {
                        NavTile navTile = ReadNavTile(reader, navMesh.IdManager, out NavPolyId tileRef);
                        navMesh.AddTileAt(navTile, tileRef);
                    }
                }
            }

            return navMesh;
        }

        private Geometry.Vector3 ReadVector3(BinaryReader reader)
        {
            Geometry.Vector3 vector3 = new Geometry.Vector3();

            vector3.X = reader.ReadSingle();
            vector3.Y = reader.ReadSingle();
            vector3.Z = reader.ReadSingle();

            return vector3;
        }
        private Geometry.Vector2i ReadVector2i(BinaryReader reader)
        {
            Geometry.Vector2i vector2i = new Geometry.Vector2i();

            vector2i.X = reader.ReadInt32();
            vector2i.Y = reader.ReadInt32();

            return vector2i;
        }

        private NavMeshGenerationSettings ReadNavMeshGenerationSettings(BinaryReader reader)
        {
            NavMeshGenerationSettings settings = new NavMeshGenerationSettings();
            settings.CellSize = reader.ReadSingle();
            settings.CellHeight = reader.ReadSingle();
            settings.MaxClimb = reader.ReadSingle();
            settings.AgentHeight = reader.ReadSingle();
            settings.AgentRadius = reader.ReadSingle();
            settings.MinRegionSize = reader.ReadInt32();
            settings.MergedRegionSize = reader.ReadInt32();
            settings.MaxEdgeLength = reader.ReadInt32();
            settings.MaxEdgeError = reader.ReadSingle();
            settings.ContourFlags = (ContourBuildFlags)reader.ReadByte();
            settings.VertsPerPoly = reader.ReadInt32();
            settings.SampleDistance = reader.ReadInt32();
            settings.MaxSampleError = reader.ReadInt32();
            settings.BuildBoundingVolumeTree = reader.ReadBoolean();
            Rect bounds = new Rect(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            settings.Bounds = bounds;

            return settings;
        }

        private PolyMesh ReadPolyMesh(BinaryReader reader)
        {
            PolyVertex[] vertices = new PolyVertex[reader.ReadInt32()];

            for (int i = 0; i < vertices.Length; i++)
                vertices[i] = ReadPolyVertex(reader);

            PolyMesh.Polygon[] polys = new PolyMesh.Polygon[reader.ReadInt32()];

            for (int i = 0; i < polys.Length; i++)
                polys[i] = ReadPolygon(reader);

            BBox3 bounds = ReadBounds(reader);
            int numVertsPerPoly = reader.ReadInt32();
            float cellSize = reader.ReadSingle();
            float cellHeight = reader.ReadSingle();
            int borderSize = reader.ReadInt32();

            return new PolyMesh(vertices, polys, bounds, numVertsPerPoly, cellSize, cellHeight, borderSize);
        }

        private PolyMeshDetail ReadPolyMeshDetail(BinaryReader reader)
        {
            MeshData[] meshes = new MeshData[reader.ReadInt32()];

            for (int i = 0; i < meshes.Length; i++)
            {
                meshes[i] = ReadDetailMesh(reader);
            }

            Vector3[] vertices = new Vector3[reader.ReadInt32()];

            for (int i = 0; i < vertices.Length; i++)
                vertices[i] = ReadVector3(reader);

            TriangleData[] tris = new TriangleData[reader.ReadInt32()];

            for (int i = 0; i < tris.Length; i++)
            {
                tris[i] = ReadTriangleData(reader);
            }


            return new PolyMeshDetail(meshes, vertices, tris);
        }

        private PolyMesh.Polygon ReadPolygon(BinaryReader reader)
        {
            int[] vertices = new int[reader.ReadInt32()];

            for (int i = 0; i < vertices.Length; i++)
                vertices[i] = reader.ReadInt32();

            int[] edges = new int[reader.ReadInt32()];

            for (int i = 0; i < edges.Length; i++)
                edges[i] = reader.ReadInt32();

            Area area = new Area(reader.ReadByte());
            RegionId regionId = new RegionId(reader.ReadInt32());

            return new PolyMesh.Polygon(vertices, edges, area, regionId);
        }

        private PathfindingCommon.NavMeshInfo ReadNavMeshInfo(BinaryReader reader)
        {
            PathfindingCommon.NavMeshInfo header = new PathfindingCommon.NavMeshInfo();
            header.X = reader.ReadInt32();
            header.Y = reader.ReadInt32();
            header.Layer = reader.ReadInt32();
            header.PolyCount = reader.ReadInt32();
            header.VertCount = reader.ReadInt32();
            header.MaxLinkCount = reader.ReadInt32();
            header.DetailMeshCount = reader.ReadInt32();
            header.DetailVertCount = reader.ReadInt32();
            header.DetailTriCount = reader.ReadInt32();
            header.BvNodeCount = reader.ReadInt32();
            header.OffMeshConCount = reader.ReadInt32();
            header.OffMeshBase = reader.ReadInt32();
            header.WalkableHeight = reader.ReadSingle();
            header.WalkableRadius = reader.ReadSingle();
            header.WalkableClimb = reader.ReadSingle();
            header.Bounds = ReadBounds(reader);
            header.BvQuantFactor = reader.ReadSingle();

            return header;
        }


        private NavTile ReadNavTile(BinaryReader reader, NavPolyIdManager idManager, out NavPolyId tileRef)
        {
            int id = reader.ReadInt32();
            tileRef = new NavPolyId(id);
            Vector2i location = ReadVector2i(reader);
            int layer = reader.ReadInt32();
            int salt = reader.ReadInt32();
            NavTile navTile = new NavTile(location, layer, idManager, tileRef);

            navTile.Salt = salt;
            navTile.Bounds = ReadBounds(reader);
            navTile.PolyCount = reader.ReadInt32();
            navTile.Polys = new NavPoly[navTile.PolyCount];

            for (int i = 0; i < navTile.PolyCount; i++)
                navTile.Polys[i] = ReadNavPoly(reader);

            navTile.Verts = new Geometry.Vector3[reader.ReadInt32()];

            for (int i = 0; i < navTile.Verts.Count(); i++)
                navTile.Verts[i] = ReadVector3(reader);

            navTile.DetailMeshes = new MeshData[reader.ReadInt32()];

            for (int i = 0; i < navTile.DetailMeshes.Count(); i++)
                navTile.DetailMeshes[i] = ReadDetailMesh(reader);

            navTile.DetailVerts = new Geometry.Vector3[reader.ReadInt32()];

            for (int i = 0; i < navTile.DetailVerts.Count(); i++)
                navTile.DetailVerts[i] = ReadVector3(reader);

            navTile.DetailTris = new TriangleData[reader.ReadInt32()];

            for (int i = 0; i < navTile.DetailTris.Count(); i++)
                navTile.DetailTris[i] = ReadTriangleData(reader);

            navTile.OffMeshConnectionCount = reader.ReadInt32();
            navTile.OffMeshConnections = new OffMeshConnection[navTile.OffMeshConnectionCount];

            for (int i = 0; i < navTile.OffMeshConnectionCount; i++)
                navTile.OffMeshConnections[i] = ReadOffMeshConnection(reader);

            var bVTreeCount = reader.ReadInt32();

            List<BVTree.Node> bvTreeNodes = new List<BVTree.Node>();

            for (int i = 0; i < bVTreeCount; i++)
                bvTreeNodes.Add(ReadBVTreeNode(reader));

            navTile.BVTree = new BVTree(bvTreeNodes);

            navTile.BvQuantFactor = reader.ReadSingle();
            navTile.BvNodeCount = reader.ReadInt32();
            navTile.WalkableClimb = reader.ReadSingle();

            return navTile;
        }


        private NavMeshBuilder ReadNavMeshBuilder(BinaryReader reader)
        {
            var header = ReadNavMeshInfo(reader);

            Vector3[] navVerts;
            NavPoly[] navPolys;
            PolyMeshDetail.MeshData[] navDMeshes;
            Vector3[] navDVerts;
            PolyMeshDetail.TriangleData[] navDTris;
            BVTree navBvTree;
            OffMeshConnection[] offMeshConnections;

            navPolys = new NavPoly[reader.ReadInt32()];
          
            for (int i = 0; i < navPolys.Length; i++)
                navPolys[i] = ReadNavPoly(reader);

            navVerts = new Geometry.Vector3[reader.ReadInt32()];

            for (int i = 0; i < navVerts.Length; i++)
                navVerts[i] = ReadVector3(reader);

            navDMeshes = new MeshData[reader.ReadInt32()];

            for (int i = 0; i < navDMeshes.Length; i++)
                navDMeshes[i] = ReadDetailMesh(reader);

            navDVerts = new Geometry.Vector3[reader.ReadInt32()];

            for (int i = 0; i < navDVerts.Length; i++)
                navDVerts[i] = ReadVector3(reader);

            navDTris = new TriangleData[reader.ReadInt32()];

            for (int i = 0; i < navDTris.Length; i++)
                navDTris[i] = ReadTriangleData(reader);

            offMeshConnections = new OffMeshConnection[reader.ReadInt32()]; 

            for (int i = 0; i < offMeshConnections.Length; i++)
                offMeshConnections[i] = ReadOffMeshConnection(reader);

            var bVTreeCount = reader.ReadInt32();

            List<BVTree.Node> bvTreeNodes = new List<BVTree.Node>();

            for (int i = 0; i < bVTreeCount; i++)
                bvTreeNodes.Add(ReadBVTreeNode(reader));

            navBvTree = new BVTree(bvTreeNodes);

            return new NavMeshBuilder(header, navVerts, navPolys, navDMeshes, navDVerts, navDTris, navBvTree, offMeshConnections);
        }

        private BVTree.Node ReadBVTreeNode(BinaryReader reader)
        {
            BVTree.Node node = new BVTree.Node();
            
            node.Bounds = new PolyBounds(ReadPolyVertex(reader), ReadPolyVertex(reader));
            node.Index = reader.ReadInt32();

            return node;
        }

        private PolyVertex ReadPolyVertex(BinaryReader reader)
        {
            PolyVertex polyVertex = new PolyVertex();
          
            polyVertex.X = reader.ReadInt32();
            polyVertex.Y = reader.ReadInt32();
            polyVertex.Z = reader.ReadInt32();

            return polyVertex;
        }

        private OffMeshConnection ReadOffMeshConnection(BinaryReader reader)
        {
            OffMeshConnection offMeshConnection = new OffMeshConnection();

            offMeshConnection.Pos0 = ReadVector3(reader);
            offMeshConnection.Pos1 = ReadVector3( reader);
            offMeshConnection.Radius = reader.ReadSingle();
            offMeshConnection.Poly = reader.ReadInt32();
            offMeshConnection.Flags = (OffMeshConnectionFlags)reader.ReadByte();
            offMeshConnection.Side = (BoundarySide)reader.ReadByte();

            return offMeshConnection;
        }

        private TriangleData ReadTriangleData(BinaryReader reader)
        {
            TriangleData triData = new TriangleData();
          
            triData.VertexHash0 = reader.ReadInt32();
            triData.VertexHash1 = reader.ReadInt32();
            triData.VertexHash2 = reader.ReadInt32();
            triData.Flags = reader.ReadInt32();

            return triData;
        }

        private MeshData ReadDetailMesh(BinaryReader reader)
        {
            MeshData meshData = new MeshData();

            meshData.VertexIndex = reader.ReadInt32();
            meshData.VertexCount = reader.ReadInt32();
            meshData.TriangleIndex = reader.ReadInt32();
            meshData.TriangleCount = reader.ReadInt32();

            return meshData;
        }

        private NavPoly ReadNavPoly(BinaryReader reader)
        {
            NavPoly navPoly = new NavPoly();

            navPoly.PolyType = (NavPolyType)reader.ReadByte();
            
            var linkCount = reader.ReadInt32();

            for (int i = 0; i < linkCount; i++)
                navPoly.Links.Add(ReadLink(reader));

            navPoly.Verts = new int[reader.ReadInt32()];

            for (int i = 0; i < navPoly.Verts.Count(); i++)
                navPoly.Verts[i] = reader.ReadInt32();

            navPoly.Neis = new int[reader.ReadInt32()];

            for (int i = 0; i < navPoly.Neis.Count(); i++)
                navPoly.Neis[i] = reader.ReadInt32();

            navPoly.VertCount = reader.ReadInt32();
            navPoly.Area = new Area(reader.ReadByte());

            return navPoly;
        }

        private Link ReadLink(BinaryReader reader)
        {
            Link link = new Link();
            link.Reference = new NavPolyId(reader.ReadInt32());
            link.Edge = reader.ReadInt32();
            link.Side = (BoundarySide)reader.ReadByte();
            link.BMin = reader.ReadInt32();
            link.BMax = reader.ReadInt32();

            return link;
        }

        private BBox3 ReadBounds(BinaryReader reader)
        {
            BBox3 bounds = new BBox3(ReadVector3(reader), ReadVector3(reader));

            return bounds;
        }
    }
}
