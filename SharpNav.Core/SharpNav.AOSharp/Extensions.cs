﻿using AOSharp.Core;
using SharpNav;
using SharpNav.Geometry;
using System;
using System.Diagnostics;

namespace AOSharp.Pathfinding
{
    public static class Extensions
    {
        public static SharpNav.Geometry.Vector3 ToSharpNav(this AOSharp.Common.GameData.Vector3 vector3) => new SharpNav.Geometry.Vector3(vector3.X, vector3.Y, vector3.Z);

        public static Common.GameData.Vector3 ToVector3(this SharpNav.Geometry.Vector3 vector3) => new AOSharp.Common.GameData.Vector3(vector3.X, vector3.Y, vector3.Z);

        public static Common.GameData.Vector3 ToVector3(this PolyVertex pv, BBox3 bounds, float cellSize, float cellHeight)
        {
            return bounds.Min.ToVector3() + new Common.GameData.Vector3(pv.X * cellSize, pv.Y * cellHeight, pv.Z * cellSize);
        }

        public static string FormatTime(this long miliseconds) => string.Format("{0:mm\\:ss\\.fff}", TimeSpan.FromMilliseconds(miliseconds));

        public static string ChangePathExtension(this string filePath, string newExtension)
        {
            string directory = System.IO.Path.GetDirectoryName(filePath);
            string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(filePath);
            string newFilePath = System.IO.Path.Combine(directory, $"{fileNameWithoutExtension}{newExtension}");

            return newFilePath;
        }

        public static long ElapsedAndReset(this Stopwatch sw)
        {
            long elapsed = sw.ElapsedMilliseconds;
            sw.Restart();

            return elapsed;
        }
    }
}