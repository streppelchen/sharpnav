﻿using AOSharp.Core;
using AOSharp.Common.GameData;
using SharpNav.Pathfinding;
using SharpNav;

namespace AOSharp.Pathfinding
{
    public struct Poly
    {
        internal NavPolyId Id { get; private set; }
        internal Area Area { get; private set; }
        internal Vector3[] Verts;
        private Vector3 _median;

        internal Poly(Vector3[] positions, NavPolyId id, Area area)
        {
            Id = id;
            Area = area;
            Verts = positions;
            _median = Vector3.Zero;

            foreach (Vector3 v in Verts)
                _median += v;

            _median /= Verts.Length;
        }

        internal void Draw(int drawDistance, Vector3 color)
        {
            if (_median.DistanceFrom(DynelManager.LocalPlayer.Position) > drawDistance)
                return;

            Draw(color);
        }

        internal void Draw(Vector3 color) => Draw(color, Vector3.Zero);

        internal void Draw(Vector3 color, Vector3 offset)
        {
            for (int i = 0; i < Verts.Length - 1; i++)
                Debug.DrawLine(Verts[i] + offset, Verts[i + 1] + offset, color);

            Debug.DrawLine(Verts[Verts.Length - 1] + offset, Verts[0] + offset, color);
        }
    }
}
