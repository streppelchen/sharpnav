﻿using AOSharp.Core;
using AOSharp.Common.GameData;
using System.Collections.Generic;
using AOSharp.Core.UI;
using SharpNav;
using SharpNav.Pathfinding;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using static SharpNav.NavMeshQuery;

namespace AOSharp.Pathfinding
{
    public class SNavAgent
    {
        public bool HasPathfinder => _pathFinder != null;
        public bool IsNavigating => Path.Count != 0;
        public NavMesh NavMesh => _pathFinder.NavMesh;

        internal Queue<Path> Path = new Queue<Path>();
        internal Path CurrentPath => Path.Peek();
        internal bool IsMoving => DynelManager.LocalPlayer.IsMoving;

        private SPathfinder _pathFinder;

        internal void LerpTowards(Vector3 pos, float minSpeed, float maxSpeed, float deltaTime)
        {
            Vector3 myPos = DynelManager.LocalPlayer.Position;
            myPos.Y = 0;
            Vector3 dstPos = pos;
            dstPos.Y = 0;

            Quaternion fromRot = DynelManager.LocalPlayer.Rotation;
            Quaternion toRot = Quaternion.FromTo(myPos, dstPos);
            float dT = deltaTime * MathExtras.Remap(DynelManager.LocalPlayer.Velocity, 0, 13, minSpeed, maxSpeed);

            DynelManager.LocalPlayer.Rotation = MathExtras.Slerp(fromRot, toRot, dT);
        }

        internal AgentNavMeshState IsOnNavPoly(float radius, out Poly poly)
        {
            return _pathFinder.IsOnNavMesh(radius, out poly);
        }

        internal Vector3 ProjectOnPolygon(Vector3 center)
        {
            return _pathFinder.ProjectOnPolygon(center);
        }

        public bool FindNearestNavPoint(Vector3 position, Vector3 extents, out NavPoint point)
        {
            point = new NavPoint();

            if (PathFinderIsNull())
                return false;

            return _pathFinder.FindNearestPoint(position, extents, out point);
        }

        internal List<Vector3> GenerateNavPath(Vector3 startPos, Vector3 endPos)
        {
            if (PathFinderIsNull())
                return new List<Vector3>();

            return _pathFinder.GeneratePath(startPos, endPos);
        }

        internal List<Vector3> GenerateSmoothNavPath(Vector3 startPos, Vector3 endPos, float density)
        {
            if (PathFinderIsNull())
                return new List<Vector3>();

            return _pathFinder.GenerateSmoothPath(startPos, endPos, density);
        }

        internal void SetSmoothNavDestination(Vector3 endPos, float density)
        {
            if (PathFinderIsNull())
                return;

            SMovementController.SetWaypoints(GenerateSmoothNavPath(DynelManager.LocalPlayer.Position, endPos, density));
        }

        internal void SetNavDestination(Vector3 endPos)
        {
            if (PathFinderIsNull())
                return;

            SMovementController.SetWaypoints(GenerateNavPath(DynelManager.LocalPlayer.Position, endPos));
        }

        internal void SetRandomDestination()
        {
            if (PathFinderIsNull())
                return;

            SMovementController.SetWaypoints(_pathFinder.GeneratePath(DynelManager.LocalPlayer.Position, GetRandomPoint()));
        }

        internal Vector3 GetRandomPoint()
        {
            return _pathFinder.GetRandomPoint().Position.ToVector3();
        }

        internal void AppendNavDestination(Vector3 startPos, Vector3 endPos)
        {
            if (PathFinderIsNull())
                return;

            SMovementController.AppendWaypoints(GenerateNavPath(startPos, endPos));
        }

        internal void AppendPath(SPath sPath)
        {
            SMovementController.AppendWaypoints(sPath.GetWaypoints(out _));
        }

        internal void SetPath(SPath sPath)
        {
            SMovementController.SetWaypoints(sPath.GetWaypoints(out _));
        }

        internal void Stop()
        {
            Path.Clear();
            SMovementController.SetMovement(MovementAction.FullStop);
        }

        internal void FollowTarget(Identity identity)
        {
            Network.Send(new FollowTargetMessage
            {
                Type = FollowTargetType.Target,
                Info = new FollowTargetMessage.TargetInfo
                {
                    Target = identity
                }
            });
        }

        internal void ResetPathfinder()
        {
            _pathFinder = null;
        }

        /// <summary>
        /// Generates a path from given coordinate and returns the path distance.
        /// </summary>
        internal bool GetDistance(Vector3 destination, out float distance)
        {
            distance = 0;

            if (PathFinderIsNull())
                return false;

            try
            {
                List<Vector3> path = _pathFinder.GeneratePath(DynelManager.LocalPlayer.Position, destination);

                for (int i = 0; i < path.Count - 1; i++)
                    distance += Vector3.Distance(path[i], path[i + 1]);

                return true;
            }
            catch
            {
                return false;
            }
        }

        internal bool TryGetNavmeshPfId(out int pfId)
        {
            pfId = 0;

            if (_pathFinder == null)
                return false;

            pfId = _pathFinder.PlayfieldId;

            return true;
        }

        /// <summary>
        /// Sets NavMesh
        /// </summary>
        internal void SetNavmesh(NavMesh navmesh)
        {
            if (navmesh == null)
                return;

            if (_pathFinder?.PlayfieldId == Playfield.ModelIdentity.Instance)
                return;

            _pathFinder = new SPathfinder(navmesh, Playfield.ModelIdentity.Instance);
        }

        /// <summary>
        /// Clears current NavMesh
        /// </summary>
        internal void DeleteNavmesh()
        {
            Stop();
            _pathFinder = null;
        }

        private bool PathFinderIsNull()
        {
            if (_pathFinder == null)
            {
                Chat.WriteLine("Pathfinder is null (navmesh was not set?)");
                return true;
            }

            return false;
        }

        internal AgentNavMeshState FindPolys(int distance, out Poly[] polies)
        {
           return _pathFinder.FindPolys(distance, out polies);
        }
    }
}
