﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOSharp.Common.GameData;
using sVector3 = SharpNav.Geometry.Vector3;
using NavmeshQuery = SharpNav.NavMeshQuery;
//using PathCorridor = SharpNav.Crowds.PathCorridor;
using SharpNav.Pathfinding;
using SharpNav;
using AOSharp.Core.UI;
using AOSharp.Core;
using static SharpNav.NavMeshQuery;
using System.IO;

namespace AOSharp.Pathfinding
{
    public class SPathfinder
    {
        internal NavMesh NavMesh;
        internal int PlayfieldId;
        private NavmeshQuery _query;
        private NavQueryFilter _filter;
        //private PathCorridor _pathCorridor;
        private Poly? _lastPoly;

        public SPathfinder(NavMesh navMesh, int playfieldId)
        {
            NavMesh = navMesh;
            PlayfieldId = playfieldId;
            _filter = new NavQueryFilter();
            _query = new NavmeshQuery(navMesh, 8192);
            //_pathCorridor = new PathCorridor();
        }

        internal AgentNavMeshState IsOnNavMesh(float radius, out Poly poly)
        {
            Vector3 startPos = DynelManager.LocalPlayer.Position;
            poly = new Poly();  

            try
            {
                if (_lastPoly != null && SharpNav.Geometry.Containment.PointInPoly(startPos, _lastPoly.Value))
                {
                    poly = _lastPoly.Value;
                    return AgentNavMeshState.InPolygon;
                }

                AgentNavMeshState agentState = IsOnNavMesh(DynelManager.LocalPlayer.Position, new Vector3(radius, 2f, radius), out poly);

                switch(agentState)
                {
                    case AgentNavMeshState.InPolygon:
                        _lastPoly = poly;
                        break;
                    default:
                        break;
                }
                return agentState;
            }
            catch
            {
                return AgentNavMeshState.OutNavMesh;
            }
        }

        internal Vector3 ProjectOnPolygon(Vector3 center)
        {
            sVector3 sCenter = center.ToSharpNav();
            sVector3 sExtents = new sVector3(50, 10, 50);

            if (_query.IsOnNavmesh(ref sCenter, ref sExtents, out sVector3[] results, out _) == AgentNavMeshState.InPolygon)
                return MathExtras.ProjectPointOntoPolygon(sCenter.ToVector3(), results.Select(x => x.ToVector3()).ToList());
            else
                return center;
        }

        internal NavPoint GetRandomPoint() => _query.FindRandomPoint();

        internal List<Vector3> GeneratePath(Vector3 start, Vector3 end)
        {
            List<Vector3> newPath = new List<Vector3>();

            if (!FindNearestPoint(start, SMovementController.Extents, out NavPoint origin) || origin.Position == new sVector3())
            {
                Chat.WriteLine("Could not find valid origin point on navmesh");
                return new List<Vector3>();
            }

            if (!FindNearestPoint(end, SMovementController.Extents, out NavPoint destination) || destination.Position == new sVector3())
            {
                Chat.WriteLine("Could not find valid destination point on navmesh");
                return new List<Vector3>();
            }

            //Chat.WriteLine($"Want path from {start} to {end}. Search Extents: {SMovementController.Extents}");
            //Chat.WriteLine($"Generating path from {origin.Position}({origin.Polygon}) to {destination.Position}({destination.Polygon})");

            SharpNav.Pathfinding.Path path = new SharpNav.Pathfinding.Path();

            if (!_query.FindPath(ref origin, ref destination, _filter, path))
                return new List<Vector3>();

            sVector3[] straightPath = StraightenPath(start.ToSharpNav(), end.ToSharpNav(), path);


            newPath.AddRange(straightPath.Select(node => new Vector3(node.X, node.Y, node.Z)));

            CullUnneededWaypoints(newPath);

            // Ensure that any distance from waypoint[i] to waypoint[i+1] doesn't exceed given threshold
            List<Vector3> finalPath = new List<Vector3>();
            float splittingPoint = 20f;

            for (int i = 0; i < newPath.Count - 1; i++)
            {
                finalPath.Add(newPath[i]);

                float distance = Vector3.Distance(newPath[i], newPath[i + 1]);

                if (distance >= splittingPoint)
                {
                    int numSegments = (int)Math.Ceiling(distance / splittingPoint);
                    Vector3 segmentDirection = (newPath[i + 1] - newPath[i]).Normalize();
                    float segmentLength = distance / numSegments;

                    for (int j = 1; j < numSegments; j++)
                    {
                        Vector3 splitPoint = newPath[i] + segmentDirection * (j * segmentLength);
                        finalPath.Add(splitPoint);
                    }
                }
            }

            finalPath.Add(newPath.Last());
            
            return finalPath;
        }

        private void CullUnneededWaypoints(List<Vector3> path)
        {
            if (path.Count < 2)
                return;

            for (int i = 0; i < path.Count - 1; i++)
            {
                var pointOnFirstSegment = MathExtras.ClosestPointOnLineSegment(path[i], path[i + 1], DynelManager.LocalPlayer.Position);

                if (DynelManager.LocalPlayer.Position.Distance2DFrom(pointOnFirstSegment) < 0.5f)
                    path.RemoveAt(i--);
            }
        }

        private List<Vector3> GenerateInterpolatedPath(Vector3 startPos, Vector3 endPos, float density)
        {
            try
            {
                List<Vector3> path = GeneratePath(startPos, endPos);
                path.Insert(0, startPos);
                return path.Count < 2 ? path : MathExtras.InterpolateXYZ(path, density);
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message);
                return new List<Vector3>();
            }
        }

        internal List<Vector3> GenerateSmoothPath(Vector3 startPos, Vector3 endPos, float density)
        {
            try
            {
                List<Vector3> initialPath = new List<Vector3>();

                foreach (var position in GenerateInterpolatedPath(startPos, endPos, density))
                {
                    if (FindNearestPoint(position, SMovementController.Extents, out NavPoint point))
                        initialPath.Add(point.Position.ToVector3());
                }

                return initialPath;
            }
            catch
            {
                return new List<Vector3>();
            }
        }

        private sVector3[] StraightenPath(sVector3 start, sVector3 end, SharpNav.Pathfinding.Path path)
        {
            StraightPath straightPath = new StraightPath();

            if (!_query.FindStraightPath(start, end, path, straightPath, PathBuildFlags.None))
                throw new Exception("Failed to straighten path.");

            List<sVector3> paths = new List<sVector3>();

            for (int i = 0; i < straightPath.Count; i++)
                paths.Add(straightPath[i].Point.Position);

            return paths.ToArray();
        }

        public bool FindNearestPoint(Vector3 position, Vector3 extents, out NavPoint point)
        {
            point = new NavPoint();

            try
            {
                sVector3 startPoint = position.ToSharpNav();
                sVector3 sExtents = extents.ToSharpNav();
                _query.FindNearestPoly(ref startPoint, ref sExtents, out point);
                return true;
            }
            catch 
            {
                return false;
            }
        }


        public AgentNavMeshState IsOnNavMesh(Vector3 position, Vector3 extents, out Poly poly)
        {
            sVector3 startPoint = position.ToSharpNav();
            sVector3 sExtents = extents.ToSharpNav();
            poly = new Poly();

            AgentNavMeshState agentState = _query.IsOnNavmesh(ref startPoint, ref sExtents, out sVector3[] result, out NavPolyId id);

            switch (agentState)
            {
                case AgentNavMeshState.InPolygon:
                    poly = new Poly(result.Select(x => x.ToVector3()).ToArray(), id, Area.Default);
                    break;
                default:
                    break;
            }

            return agentState;
        }

        public bool IsUsingNavmesh(TiledNavMesh navmesh)
        {
            return navmesh == NavMesh;
        }

        internal AgentNavMeshState FindPolys(int distance, out Poly[] polies)
        {
            sVector3 center = DynelManager.LocalPlayer.Position.ToSharpNav();
            sVector3 extents = new sVector3(distance, 20, distance);

            return _query.FindPolygons(ref center, ref extents, out polies);
        }
    }
}