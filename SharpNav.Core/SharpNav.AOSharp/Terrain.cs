﻿

using AOSharp.Common.GameData;
using AOSharp.Common.Unmanaged.DbObjects;
using AOSharp.Core;
using System.Collections.Generic;
using System.Linq;
using System;

namespace AOSharp.Pathfinding
{
    public class DungeonTerrain
    {
        public static List<Mesh> CreateFromCurrentPlayfield(bool playerFloorOnly)
        {
            if (!Playfield.IsDungeon)
            {
                throw new Exception("Must be in a dungeon!");
            }

            List<Mesh> list = new List<Mesh>();
            DungeonRDBTilemap tilemap = Playfield.RDBTilemap as DungeonRDBTilemap;

            if (playerFloorOnly)
            {
                foreach (Room room in Playfield.Rooms.Where(x => x.Floor == DynelManager.LocalPlayer.Room.Floor))
                {
                    list.Add(CreateMesh(room, tilemap));
                }
            }
            else
            {
                foreach (Room room in Playfield.Rooms)
                {
                    list.Add(CreateMesh(room, tilemap));
                }
            }

            return list;
        }


        public static Mesh CreateMesh(Room room, DungeonRDBTilemap tilemap)
        {
            int numWidthTiles = (int)room.LocalRect.MaxX - (int)room.LocalRect.MinX;
            int numHeightTiles = (int)room.LocalRect.MaxY - (int)room.LocalRect.MinY;
            float width = numWidthTiles * tilemap.TileSize;
            float length = numHeightTiles * tilemap.TileSize;
            int hCount = numWidthTiles + 1;
            int vCount = numHeightTiles + 1;
            int numTriangles = numWidthTiles * numHeightTiles * 6;
            int numVertices = hCount * vCount;

            Vector3[] vertices = new Vector3[numVertices];
            List<int> indices = new List<int>();

            Vector3 anchorOffset = new Vector3(1, 0, 1);
            anchorOffset.X += (room.Center.X - (float)numWidthTiles / 2) * tilemap.TileSize;
            anchorOffset.Z += (room.Center.Z - (float)numHeightTiles / 2) * tilemap.TileSize;

            int idx = 0;
            for (int z = 0; z < vCount; z++)
            {
                for (int x = 0; x < hCount; x++)
                {
                    byte height = tilemap.Heightmap[x + (int)room.LocalRect.MinX - 1, z + (int)room.LocalRect.MinY - 1];

                    Vector3 vertex = new Vector3
                    {
                        X = x * tilemap.TileSize - width / 2f,
                        Y = (float)height * tilemap.HeightmapScale,
                        Z = z * tilemap.TileSize - length / 2f
                    };
                    vertex.X -= anchorOffset.X;
                    vertex.Z -= anchorOffset.Z;
                    vertices[idx] = vertex;
                    idx++;
                }
            }

            idx = 0;
            for (int z = 0; z < numHeightTiles; z++)
            {
                for (int x = 0; x < numWidthTiles; x++)
                {
                    byte tileCollisionData = tilemap.CollisionData[x + (int)room.LocalRect.MinX, z + (int)room.LocalRect.MinY];

                    if (tileCollisionData > 0 && tileCollisionData != 0x80)
                    {
                        indices.Add((z * hCount) + x);
                        indices.Add(((z + 1) * hCount) + x);
                        indices.Add((z * hCount) + x + 1);

                        indices.Add(((z + 1) * hCount) + x);
                        indices.Add(((z + 1) * hCount) + x + 1);
                        indices.Add((z * hCount) + x + 1);
                    }
                    idx += 6;
                }
            }


            List<Vector3> optimizedVerts = vertices.ToList();
            List<int> optimizedIndices = indices;

            int testVertex = 0;

            while (testVertex < optimizedVerts.Count)
            {
                if (optimizedIndices.Contains(testVertex))
                {
                    testVertex++;
                }
                else
                {
                    optimizedVerts.RemoveAt(testVertex);

                    for (int i = 0; i < optimizedIndices.Count; i++)
                    {
                        if (optimizedIndices[i] > testVertex)
                            optimizedIndices[i]--;
                    }
                }
            }

            return new Mesh
            {
                Triangles = optimizedIndices,
                Vertices = optimizedVerts,
                Position = room.Position - new Vector3(0, room.YOffset, 0),
                Rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, room.Rotation * (Math.PI / 180)),
                Scale = new Vector3(1, 1, 1)
            };
        }
    }


    public class Terrain
    {
        public static List<Mesh> CreateFromCurrentPlayfield()
        {
            if (Playfield.IsDungeon)
            {
                throw new Exception("Must be outdoors!");
            }

            List<Mesh> list = new List<Mesh>();
            OutdoorRDBTilemap outdoorRDBTilemap = Playfield.RDBTilemap as OutdoorRDBTilemap;
            foreach (OutdoorRDBTilemap.Chunk chunk in outdoorRDBTilemap.Chunks)
            {
                list.Add(CreateMesh(chunk, outdoorRDBTilemap.TileSize, outdoorRDBTilemap.HeightmapScale));
            }

            return list;
        }

        public static List<Mesh> CreateFromTilemap(OutdoorRDBTilemap tilemap)
        {
            List<Mesh> list = new List<Mesh>();
            foreach (OutdoorRDBTilemap.Chunk chunk in tilemap.Chunks)
            {
                list.Add(CreateMesh(chunk, tilemap.TileSize, tilemap.HeightmapScale));
            }

            return list;
        }

        private static Mesh CreateMesh(OutdoorRDBTilemap.Chunk chunk, float tileSize, float heightMapScale)
        {
            int num = 0;
            int num2 = 0;
            Vector3[] array = new Vector3[chunk.Size * chunk.Size];
            for (int i = 0; i < chunk.Size; i++)
            {
                for (int j = 0; j < chunk.Size; j++)
                {
                    array[num] = new Vector3((float)j * tileSize, (float)(int)chunk.Heightmap[j, i] * heightMapScale, (float)i * tileSize);
                    num++;
                }
            }

            List<int> list = new List<int>();
            for (int k = 0; k < chunk.Size - 1; k++)
            {
                for (int l = 0; l < chunk.Size - 1; l++)
                {
                    list.Add(k * chunk.Size + l + 1);
                    list.Add(k * chunk.Size + l);
                    list.Add((k + 1) * chunk.Size + l);
                    list.Add(k * chunk.Size + l + 1);
                    list.Add((k + 1) * chunk.Size + l);
                    list.Add((k + 1) * chunk.Size + l + 1);
                    num2 += 6;
                }
            }

            return new Mesh
            {
                Triangles = list,
                Vertices = array.ToList(),
                Position = new Vector3((float)(chunk.X * (chunk.Size - 1)) * tileSize, 0f, (float)(chunk.Y * (chunk.Size - 1)) * tileSize),
                Rotation = Quaternion.Identity,
                Scale = new Vector3(1f, 1f, 1f)
            };
        }
    }
}