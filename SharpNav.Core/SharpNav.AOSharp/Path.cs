﻿using AOSharp.Core;
using AOSharp.Common.GameData;
using System.Collections.Generic;
using System;
using System.Linq;

namespace AOSharp.Pathfinding
{
    public class SNavMeshPath : Path
    {
        private SPathfinder _pathfinder;
        public readonly Vector3 Destination;

        public SNavMeshPath(SPathfinder pathfinder, Vector3 dstPos, float nodeReachedDist, float destinationReachedDist) : base(new List<Vector3>(), nodeReachedDist, destinationReachedDist)
        {
            _pathfinder = pathfinder;
            Destination = dstPos;
        }

        public override void OnPathStart()
        {
            base.OnPathStart();
        }

        public override void OnPathFinished()
        {
            base.OnPathFinished();
        }
    }

    public class Path
    {
        private float _nodeReachedDist;
        private float _destinationReachedDist;
        public bool StopAtDest = true;
        public bool Initialized = false;
        public Queue<Vector3> Waypoints = new Queue<Vector3>();
        public Action DestinationReachedCallback;

        public Path(Vector3 destination, float nodeReachedDist, float destinationReachedDist) : this(new List<Vector3>() { destination }, nodeReachedDist, destinationReachedDist)
        {
        }

        public Path(List<Vector3> waypoints, float nodeReachedDist, float destinationReachedDist)
        {
            _nodeReachedDist = nodeReachedDist;
            _destinationReachedDist = destinationReachedDist;

            SetWaypoints(waypoints);
        }

        public virtual void OnPathStart()
        {
            Initialized = true;
        }

        public virtual void OnPathFinished()
        {
            DestinationReachedCallback?.Invoke();
        }


        protected void SetWaypoints(List<Vector3> waypoints)
        {
            Waypoints.Clear();

            int count = 0;

            for (int i = 0; i < waypoints.Count; i++)
            {
                var currWaypoint = waypoints[i];

                if (DynelManager.LocalPlayer.Position.DistanceFrom(currWaypoint) <= _nodeReachedDist && count == i)
                {
                    count++;
                    continue;
                }

                Waypoints.Enqueue(currWaypoint);
            }
        }

        internal bool PeekLastWaypoint(out Vector3 waypoint)
        {
            if (Waypoints.Count == 0)
            {
                waypoint = Vector3.Zero;
                return false;
            }

            waypoint = Waypoints.LastOrDefault();
            return true;
        }

        internal bool GetNextWaypoint(out Vector3 waypoint)
        {
            if (Waypoints.Count == 0)
            {
                waypoint = Vector3.Zero;
                return false;
            }

            Vector3 playerPos = DynelManager.LocalPlayer.Position;
            playerPos.Y = 0f;

            while (Waypoints.Count > 0)
            {
                Vector3 targetPos = Waypoints.Peek();
                targetPos.Y = 0f;

                if (playerPos.DistanceFrom(targetPos) >= (Waypoints.Count > 1 ? _nodeReachedDist : _destinationReachedDist))
                {
                    waypoint = Waypoints.Peek();
                    return true;
                }
                else
                {
                    Waypoints.Dequeue();
                }
            }

            waypoint = Vector3.Zero;
            return false;
        }

        internal float Length()
        {
            Vector3 lastWaypoint = DynelManager.LocalPlayer.Position;
            float pathLength = 0.0f;

            foreach (Vector3 waypoint in Waypoints)
            {
                pathLength += Vector3.Distance(lastWaypoint, waypoint);
                lastWaypoint = waypoint;
            }

            return pathLength;
        }

        internal void Draw()
        {
            Vector3 lastWaypoint = DynelManager.LocalPlayer.Position;

            foreach (Vector3 waypoint in Waypoints)
            {
                Debug.DrawLine(lastWaypoint, waypoint, DebuggingColor.Yellow);
                lastWaypoint = waypoint;
            }
        }
    }
}
