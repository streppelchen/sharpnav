﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using SharpNav.Pathfinding;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AOSharp.Pathfinding
{
    public enum SPathLinkType
    {
        TeleportPad,
        UseTerminal
    }

    public class SLink
    {
        public SPath Link1 { internal set; get; }
        public SPath Link2 { internal set; get; }
        public Vector3 Link1Pos { get; }
        public Vector3 Link2Pos { get; }

        public SPathLinkType LinkType { get; }

        public SLink(SPath link1, SPath link2, SPathLinkType linkType)
        {
            Link1 = link1;
            Link2 = link2;
            LinkType = linkType;

            if (link1.Waypoints.Count == 0)
            {
                Chat.WriteLine("SPath 1 has no waypoints, could not create link");
                return;
            }

            if (link2.Waypoints.Count == 0)
            {
                Chat.WriteLine("SPath 2 has no waypoints, could not create link");
                return;
            }

            Link1Pos = link1.Waypoints.LastOrDefault();
            Link2Pos = link2.Waypoints.FirstOrDefault();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            SLink other = (SLink)obj;
            return (Link1 == other.Link1 && Link2 == other.Link2) || (Link1 == other.Link2 && Link2 == other.Link1);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + Link1.GetHashCode();
                hash = hash * 23 + Link2.GetHashCode();
                return hash;
            }
        }
    }
}