﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using SharpNav.Pathfinding;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;

namespace AOSharp.Pathfinding
{
    public class SPathManager
    {
        private static List<SPath> _paths;
        private static List<SLink> _links;

        public static IReadOnlyCollection<SPath> Paths => _paths;
        private static bool _loaded = false;

        public static IReadOnlyCollection<SRoute> Routes => _routes;
        private static List<SRoute> _routes;
        private static SRoute _activeRoute;

        internal static void Set()
        {
            if (_loaded)
                return;

            _paths = new List<SPath>();
            _links = new List<SLink>();
            _routes = new List<SRoute>();

            Game.OnUpdate += OnUpdate;
            Network.N3MessageReceived += OnN3TeleportMessageReceived;

            _loaded = true;
        }

        private static void OnUpdate(object sender, float e)
        {
            foreach (var path in _paths)
            {
                if (path == null || path.Waypoints == null || path.Waypoints.Count == 0)
                    continue;

                if (!path.ShouldDraw)
                    continue;

                if (path.PlayfieldId != Playfield.ModelIdentity.Instance)
                    continue;

                Draw(path);
            }

            foreach (var link in _links)
            {
                if (link.Link1?.Waypoints.Count == 0 || link.Link2?.Waypoints?.Count == 0)
                    continue;

                if (link.Link1.PlayfieldId == link.Link2.PlayfieldId && link.Link2.PlayfieldId ==  Playfield.ModelIdentity.Instance )
                    Debug.DrawLine(link.Link1Pos, link.Link2Pos, DebuggingColor.Purple);
                else if (link.Link1.PlayfieldId == Playfield.ModelIdentity.Instance)
                    Debug.DrawLine(link.Link1Pos, link.Link1Pos + Vector3.Up, DebuggingColor.Purple);
                else if (link.Link2.PlayfieldId == Playfield.ModelIdentity.Instance) 
                    Debug.DrawLine(link.Link2Pos, link.Link2Pos + Vector3.Up, DebuggingColor.Purple);
            }
        }

        private static void OnN3TeleportMessageReceived(object sender, N3Message n3Msg)
        {
            if (!(n3Msg is N3TeleportMessage n3Teleport))
                return;

            if (n3Msg.Identity != DynelManager.LocalPlayer.Identity)
                return;

            if (_activeRoute == null)
                return;

            var link = _activeRoute.Links
                .Where(x => { return Vector3.Distance(n3Teleport.Destination, x.Link2Pos) < 20f; })
                .OrderBy(distance => distance)
                .FirstOrDefault();

            if (link == null)
                return;

            SMovementController.SetPath(link.Link2);
        }

        public static void CreateRoute(string routeName, List<SLink> links)
        {
            if (_routes.Any(x => x.Name == routeName))
            {
                Chat.WriteLine("Route name already exists");
                return;
            }

            try
            {
                for (int i = 0; i < links.Count; i++)
                {
                    var link = _links.FirstOrDefault(x => x == links[i]);

                    if (link == null)
                        _links.Add(links[i]);
                    else
                        _links[i] = link;

                    var sPath1 = _paths.FirstOrDefault(x => x == links[i].Link1);

                    if (sPath1 == null)
                        _paths.Add(links[i].Link1);
                    else
                        links[i].Link1 = sPath1;

                    var sPath2 = _paths.FirstOrDefault(x => x == links[i].Link2);

                    if (sPath2 == null)
                        _paths.Add(links[i].Link2);
                    else
                        links[i].Link2 = sPath2;
                }
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message);
            }

            _routes.Add(new SRoute(routeName, links));
        }

        public static void SetActiveRoute(SRoute route, bool travelMode = false)
        {
            _activeRoute = route;

            if (route == null)
            {
                Chat.WriteLine("SRoute is null", ChatColor.Red);
                return;
            }

            if (travelMode)
            {
                SMovementController.SetPath(route.Links[0].Link1);
            }
        }

        public static void TraverseActiveRoute()
        {
            if (_activeRoute == null)
                return;

            List<SPath> allPaths = new List<SPath>();

            allPaths.AddRange(_activeRoute.Links.Select(x => x.Link1));
            allPaths.AddRange(_activeRoute.Links.Select(x => x.Link2));

            Vector3 closestWaypoint = new Vector3(1, 1, 1) * 10000;
            SPath closestPath = null;

            foreach (var path in allPaths)
            {
                foreach (var waypoint in path.GetWaypoints(out _))
                {
                    if (Vector3.Distance(waypoint, DynelManager.LocalPlayer.Position) > Vector3.Distance(closestWaypoint, DynelManager.LocalPlayer.Position))
                        continue;

                    closestWaypoint = waypoint;
                    closestPath = path;
                }
            }
            SMovementController.SetPath(closestPath,true);
        }

        internal static void Register(SPath path)
        {
            if (!_loaded)
            {
                Set();
                _loaded = true;
            }

            if (_paths.Contains(path))
            {
                Chat.WriteLine("Prevented from loading same SPath twice", ChatColor.Red);
                return;
            }

            _paths.Add(path);
        }

        internal static void Remove(SPath path)
        {
            _paths.Remove(path);
        }

        private static void Draw(SPath sPath)
        {
            var interpolatedPoints = sPath.GetWaypoints(out var newPoints);
            var pts = newPoints.OrderBy(x => Vector3.Distance(DynelManager.LocalPlayer.Position, x));
            var closestPt = pts.FirstOrDefault();

            foreach (var point in pts)
            {
                Debug.DrawSphere(point, 0.25f, point == closestPt ? DebuggingColor.Green : DebuggingColor.White);

                if (sPath.SelectedPoints.Contains(point))
                    Debug.DrawSphere(point, 0.1f, DebuggingColor.Purple);
            }

            float arrowheadLength = 0.2f;
            var arrowDir = sPath.IsReversed ? -1 : 1;

            for (int i = 0; i < interpolatedPoints.Count - 1; i++)
            {
                Debug.DrawLine(interpolatedPoints[i], interpolatedPoints[i + 1], DebuggingColor.Yellow);

                var dir = Vector3.Normalize(interpolatedPoints[i + 1] - interpolatedPoints[i])* arrowDir;
                var arrowPos = dir * arrowheadLength;

                Vector3 perpendicular1 = Vector3.Cross(dir, Vector3.Up).Normalize();

                Vector3 arrowPos1 = interpolatedPoints[i] - arrowPos + perpendicular1 * arrowheadLength;
                Vector3 arrowPos2 = interpolatedPoints[i] - arrowPos - perpendicular1 * arrowheadLength;

                Debug.DrawLine(interpolatedPoints[i], arrowPos1, DebuggingColor.Yellow);
                Debug.DrawLine(interpolatedPoints[i], arrowPos2, DebuggingColor.Yellow);
            }
        }
    }
}