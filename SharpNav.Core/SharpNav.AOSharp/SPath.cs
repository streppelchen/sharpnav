﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AOSharp.Pathfinding
{
    public class SPath
    {
        public List<Vector3> Waypoints;
        public string Name;
        public int PlayfieldId;
        public bool IsLooping;
        public bool IsReversed;

        [JsonIgnore]
        public bool IsLocked;

        [JsonIgnore]
        public bool ShouldDraw;

        [JsonIgnore]
        public int Id;

        [JsonIgnore]
        internal List<Vector3> SelectedPoints;

        [JsonIgnore]
        private int? _takenPoint;

        [JsonIgnore]
        private Vector3 _playerPos => Vector3.Up * 0.1f + DynelManager.LocalPlayer.Position;

        private SPath()
        {
            PlayfieldId = Playfield.ModelIdentity.Instance;
            Waypoints = new List<Vector3>();
            SelectedPoints = new List<Vector3>();
            IsLooping = false;
            IsReversed = false;
            IsLocked = false;
            ShouldDraw = true;
        }

        public static SPath Load(string path, bool shouldDraw = true)
        {
            try
            {
                SPath sPath = JsonConvert.DeserializeObject<SPath>(File.ReadAllText(path));
                sPath.IsLocked = true;
                sPath.ShouldDraw = shouldDraw;
                SPathManager.Register(sPath);
                return sPath;
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message, ChatColor.Red);
                return null;
            }
        }

        public void Export(string path)
        {
            try
            {
                File.WriteAllText(path, JsonConvert.SerializeObject(this, Formatting.Indented));
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message, ChatColor.Red);
            }
        }

        public static SPath Create(bool shouldDraw = true)
        {
            SPath sPath = new SPath();
            sPath.ShouldDraw = shouldDraw;
            SPathManager.Register(sPath);
            return sPath;
        }


        public void Delete()
        {
            SPathManager.Remove(this);
        }

        public void SelectPoint()
        {
            var pt = GetClosestPoint();

            if (SelectedPoints.Contains(pt))
                return;

            SelectedPoints.Add(pt);
        }

        public void UnselectPoint()
        {
            var pt = GetClosestPoint();

            if (!SelectedPoints.Contains(pt))
                return;

            SelectedPoints.Remove(pt);
        }

        public List<Vector3> GetWaypoints(out List<Vector3> nodePoints)
        {
            nodePoints = Waypoints.ToList();

            var interpolatedPoints = new List<Vector3> { };

            if (_takenPoint != null)
                nodePoints[_takenPoint.Value] = _playerPos;

            if (!IsLocked)
                nodePoints.Add(_playerPos);

            if (IsLooping)
                nodePoints.Add(_takenPoint == 0 ? _playerPos : Waypoints[0]);

            if (nodePoints.Count == 1 || nodePoints.Count == 2)
            {
                interpolatedPoints.AddRange(nodePoints);
            }
            else
            {
                interpolatedPoints.Insert(0, _takenPoint != null && _takenPoint.Value == 0 ? _playerPos : Waypoints[0]);
                interpolatedPoints.AddRange(MathExtras.InterpolateXYZ(nodePoints, 1.25f));
            }

            return interpolatedPoints;
        }

        public void ToggleLock()
        {
            IsLocked = !IsLocked;
        }

        public void PickupPoint()
        {
            if (_takenPoint != null)
                return;

            if (!IsLocked)
                IsLocked = true;

            _takenPoint = Waypoints.IndexOf(GetClosestPoint());
        }

        public bool Split()
        {
            if (SelectedPoints.Count != 2)
                return false;

            var i1 = Waypoints.IndexOf(SelectedPoints[0]);
            var i2 = Waypoints.IndexOf(SelectedPoints[1]);

            if (Math.Abs(i2 - i1) != 1)
                return false;

            Waypoints.Insert(Math.Min(i1, i2) + 1, _playerPos);
            return true;
        }

        public void ToggleLoop()
        {
            IsLooping = !IsLooping;
        }

        public void Reverse()
        {
            IsReversed = !IsReversed;
        }

        public void ToggleDraw()
        {
            ShouldDraw = !ShouldDraw;
        }

        public void PlacePoint()
        {
            if (_takenPoint == null)
                return;

            Waypoints[_takenPoint.Value] = _playerPos;
            _takenPoint = null;
        }

        public void AddPoint()
        {
            Waypoints.Add(_playerPos + DynelManager.LocalPlayer.Rotation * Vector3.Forward * 0.01f);
        }

        public void AddPoint(Vector3 position)
        {
            Waypoints.Add(position);
        }

        public void AddPoints(IEnumerable<Vector3> positions)
        {
            Waypoints.AddRange(positions);
        }

        public void RemovePoint(Vector3 position)
        {
            Waypoints.Remove(position);
        }

        public void RemovePoint()
        {
            var closestPoint = GetClosestPoint();

            if (closestPoint != null)
                Waypoints.Remove(closestPoint);
        }

        private Vector3 GetClosestPoint()
        {
            return Waypoints.OrderBy(x => Vector3.Distance(DynelManager.LocalPlayer.Position, x)).FirstOrDefault();
        }

        public void Clear()
        {
            Waypoints.Clear();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            SPath other = (SPath)obj;

            return Waypoints.SequenceEqual(other.Waypoints) && PlayfieldId == other.PlayfieldId;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                foreach (var point in Waypoints)
                {
                    hash = hash * 23 + point.GetHashCode();
                }
                hash = hash * 23 + PlayfieldId.GetHashCode();
                return hash;
            }
        }
    }
}