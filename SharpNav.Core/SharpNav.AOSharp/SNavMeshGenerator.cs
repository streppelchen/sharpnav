﻿using AOSharp.Core;
using AOSharp.Common.GameData;
using System.Linq;
using System.Collections.Generic;
using System;
using AOSharp.Core.UI;
using SharpNav;
using System.Diagnostics;
using STriangle3 = SharpNav.Geometry.Triangle3;
using System.Threading.Tasks;
using SharpNav.Geometry;
using System.IO;
using SharpNav.Pathfinding;
using System.Reflection.Emit;
using AVector3 = AOSharp.Common.GameData.Vector3;
using SVector3 = SharpNav.Geometry.Vector3;

namespace AOSharp.Pathfinding
{
    public class SNavMeshGenerator
    {
        public static Action<BakeState> BakeStatus;

        public static bool GenerateFromObj(NavMeshGenerationSettings settings, string filePath, out NavMesh navMesh)
        {
            navMesh = null;

            try
            {
                if (!File.Exists(filePath))
                {
                    Console.WriteLine("File doesn't exist at given path");
                    return false;
                }

                navMesh = Generate(ObjParser.ReadObjFile(filePath), settings);

                return navMesh != null;
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
		/// An alternative way to generate dungeon navmeshes with some filtering options.
		/// </summary>
        public async static Task<NavMesh> GenerateDungeonAsync(NavMeshGenerationSettings settings, bool playerFloorOnly)
        {
            if (!Playfield.IsDungeon)
                return null;

            try
            {
                return await GenerateAsync(settings, playerFloorOnly);
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message, ChatColor.Red);
                return null;
            }
        }

        /// <summary>
        /// The main method to generate navmeshes of any map type.
        /// </summary>
        public async static Task<NavMesh> GenerateAsync(NavMeshGenerationSettings settings)
        {
            try
            {
                return await GenerateAsync(settings, false);
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message, ChatColor.Red);
                return null;
            }
        }

        private async static Task<NavMesh> GenerateAsync(NavMeshGenerationSettings settings, bool playerFloorOnly)
        {
            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                BakeStatus?.Invoke(BakeState.Generating);
                var triGeom = await Task.Run(() => TerrainData.GetTriGeometry(settings.Bounds, playerFloorOnly));
                var navMeshBake = await Task.Run(() => Generate(triGeom, settings));

                Chat.WriteLine($"Total time: {sw.ElapsedMilliseconds.FormatTime()}", ChatColor.Green);
                sw.Stop();

                BakeStatus?.Invoke(BakeState.Finished);

                return navMeshBake;
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message, ChatColor.Red);
                return null;
            }
        }

        public static bool GenerateDungeon(NavMeshGenerationSettings settings, bool playerFloorOnly, out NavMesh navMesh)
        {
            return Generate(settings, playerFloorOnly, out navMesh);
        }

        public static bool Generate(NavMeshGenerationSettings settings, out NavMesh navMesh)
        {
            return Generate(settings, false, out navMesh);
        }

        private static bool Generate(NavMeshGenerationSettings settings, bool playerFloorOnly, out NavMesh navMesh)
        {
            Stopwatch sw = Stopwatch.StartNew();
            navMesh = null;

            try
            {
                List<STriangle3> triMesh = TerrainData.GetTriGeometry(settings.Bounds, playerFloorOnly);

                navMesh = Generate(triMesh, settings);

                Chat.WriteLine($"Navmesh generation finished.", ChatColor.Green);
                Chat.WriteLine($"Total time: {sw.ElapsedMilliseconds.FormatTime()}", ChatColor.Green);
                sw.Stop();

                return true;
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message, ChatColor.Red);
                return false;
            }
        }

        public static bool GenerateBuilder(NavMeshGenerationSettings settings, Room room, out NavMeshBuilder builder)
        {
            builder = null;

            try
            {
                List<STriangle3> triMesh = TerrainData.GetTriGeometry(room);
                List<OffMeshConnection> offMeshConnections = new List<OffMeshConnection>();

                for (int i = 0; i < room.NumDoors; i++)
                {
                    //if (room.Instance != 6)
                    //    break;

                    //if (i != 1)
                    //    continue;

                    room.GetDoorPosRot(i, out var pos, out var rot);

                    offMeshConnections.Add(new OffMeshConnection
                    {
                        Pos0 = (pos + rot.Forward).ToSharpNav(),
                        Pos1 = (pos - rot.Forward).ToSharpNav(),
                        Flags = OffMeshConnectionFlags.None,
                        Radius = 0.25f
                    });
                }

                triMesh = triMesh.Select(x => new STriangle3
                {
                    A = (Quaternion.AngleAxis(360 - room.Rotation, AVector3.Up) * (x.A.ToVector3() - room.Position)).ToSharpNav(),
                    B = (Quaternion.AngleAxis(360 - room.Rotation, AVector3.Up) * (x.B.ToVector3() - room.Position)).ToSharpNav(),
                    C = (Quaternion.AngleAxis(360 - room.Rotation, AVector3.Up) * (x.C.ToVector3() - room.Position)).ToSharpNav(),
                }).ToList();

                foreach (OffMeshConnection con in offMeshConnections)
                {
                    con.Pos0 = (Quaternion.AngleAxis(360 - room.Rotation, AVector3.Up) * (con.Pos0.ToVector3() - room.Position)).ToSharpNav();
                    con.Pos1 = (Quaternion.AngleAxis(360 - room.Rotation, AVector3.Up) * (con.Pos1.ToVector3() - room.Position)).ToSharpNav();
                }

                builder = GenerateBuilder(triMesh, offMeshConnections.ToArray(), settings);

                return true;
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message, ChatColor.Red);
                return false;
            }
        }

        public static void GeneratePolyMesh(IEnumerable<STriangle3> triangles, NavMeshGenerationSettings settings, out PolyMesh polyMesh, out PolyMeshDetail detailMesh)
        {
            Stopwatch sw = Stopwatch.StartNew();

            BBox3 bounds = triangles.GetBoundingBox(settings.CellSize);

            Chat.WriteLine($"Loaded bounds. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

            Chat.WriteLine($"Bounds: {bounds}", ChatColor.LightBlue);
            var hf = new Heightfield(bounds, settings);

            Chat.WriteLine($"Pre-Rasterizing Triangles. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);
            hf.RasterizeTriangles(triangles);

            Chat.WriteLine($"Rasterizing Triangles. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

            hf.FilterLedgeSpans(settings.VoxelAgentHeight, settings.VoxelMaxClimb);
            hf.FilterLowHangingWalkableObstacles(settings.VoxelMaxClimb);
            hf.FilterWalkableLowHeightSpans(settings.VoxelAgentHeight);

            Chat.WriteLine($"Loaded heightfield. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

            var chf = new CompactHeightfield(hf, settings);
            chf.Erode(settings.VoxelAgentRadius);
            chf.BuildDistanceField();
            chf.BuildRegions(2, settings.MinRegionSize, settings.MergedRegionSize, settings.FilterLargestSection);

            Chat.WriteLine($"Loaded compact heightfield. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

            var cont = chf.BuildContourSet(settings);
            polyMesh = new PolyMesh(cont, settings);
            detailMesh = new PolyMeshDetail(polyMesh, chf, settings);
        }

        public static NavMeshBuilder GenerateBuilder(IEnumerable<STriangle3> triangles, OffMeshConnection[] offMeshConnections, NavMeshGenerationSettings settings)
        {
            GeneratePolyMesh(triangles, settings, out PolyMesh polyMesh, out PolyMeshDetail detailMesh);
            return new NavMeshBuilder(polyMesh, detailMesh, offMeshConnections, settings);
        }

        /// <summary>
		/// Generates a <see cref="NavMesh"/> given a collection of triangles and some settings.
		/// </summary>
		/// <param name="triangles">The triangles that form the level.</param>
		/// <param name="settings">The settings to generate with.</param>
		/// <returns>A <see cref="NavMesh"/>.</returns>
		private static NavMesh Generate(IEnumerable<STriangle3> triangles, NavMeshGenerationSettings settings)
        {
            NavMeshBuilder builder = GenerateBuilder(triangles, new OffMeshConnection[0], settings);
            var navMesh = new NavMesh(builder);
            navMesh.Settings = settings;

            return navMesh;
        }
    }

    public enum BakeState
    {
        Generating,
        Finished
    }
}