﻿using AOSharp.Common.GameData;
using AOSharp.Common.Unmanaged.DbObjects;
using AOSharp.Core;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json.Linq;

namespace AOSharp.Pathfinding
{
    internal class MathExtras
    {
        internal static Quaternion Slerp(Quaternion from, Quaternion to, float t)
        {
            // Clamp 't' between 0 and 1
            t = t < 0 ? 0 : t > 1 ? 1 : t;

            // Calculate the angle between the quaternions
            float dot = from.X * to.X + from.Y * to.Y + from.Z * to.Z + from.W * to.W;

            // If the dot product is negative, invert one of the quaternions
            if (dot < 0.0f)
            {
                to = new Quaternion(-to.X, -to.Y, -to.Z, -to.W);
                dot = -dot;
            }

            // If the quaternions are close together, use Lerp to avoid division by zero
            const float epsilon = 0.9995f;
            if (dot > epsilon)
            {
                return new Quaternion(
                    from.X + (to.X - from.X) * t,
                    from.Y + (to.Y - from.Y) * t,
                    from.Z + (to.Z - from.Z) * t,
                    from.W + (to.W - from.W) * t
                );
            }

            // Calculate the angle between the quaternions
            float theta = (float)System.Math.Acos(dot);
            float sinTheta = (float)System.Math.Sin(theta);

            // Perform the Slerp interpolation
            float invSinTheta = 1.0f / sinTheta;
            float coeff0 = (float)System.Math.Sin((1.0f - t) * theta) * invSinTheta;
            float coeff1 = (float)System.Math.Sin(t * theta) * invSinTheta;

            return new Quaternion(
                coeff0 * from.X + coeff1 * to.X,
                coeff0 * from.Y + coeff1 * to.Y,
                coeff0 * from.Z + coeff1 * to.Z,
                coeff0 * from.W + coeff1 * to.W
            );
        }

        internal static float Dot(Quaternion a, Quaternion b) => a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * b.W;

        internal static float Clamp(float value, float min, float max) => value < min ? min : value > max ? max : value;

        internal static float Remap(float value, float from1, float to1, float from2, float to2) => from2 + (value - from1) * (to2 - from2) / (to1 - from1);

        public static List<Vector3> InterpolateXYZ(List<Vector3> points, double targetSpacing)
        {
            double[] xs = points.Select(x => (double)x.X).ToArray();
            double[] ys = points.Select(x => (double)x.Y).ToArray();
            double[] zs = points.Select(x => (double)x.Z).ToArray();
            int inputPointCount = xs.Length;

            double totalDistance = 0.0;

            for (int i = 1; i < inputPointCount; i++)
            {
                double dx = xs[i] - xs[i - 1];
                double dy = ys[i] - ys[i - 1];
                double dz = zs[i] - zs[i - 1];
                totalDistance += Math.Sqrt(dx * dx + dy * dy + dz * dz);
            }

            int count = (int)Math.Ceiling(totalDistance / targetSpacing) + 1;

            double[] inputDistances = new double[inputPointCount];

            for (int i = 1; i < inputPointCount; i++)
            {
                double dx = xs[i] - xs[i - 1];
                double dy = ys[i] - ys[i - 1];
                double dz = zs[i] - zs[i - 1];
                double distance = Math.Sqrt(dx * dx + dy * dy + dz * dz);
                inputDistances[i] = inputDistances[i - 1] + distance;
            }

            double meanDistance = totalDistance / (count - 1);
            double[] evenDistances = Enumerable.Range(0, count).Select(x => x * meanDistance).ToArray();
            double[] xsOut = Interpolate(inputDistances, xs, evenDistances);
            double[] ysOut = Interpolate(inputDistances, ys, evenDistances);
            double[] zsOut = Interpolate(inputDistances, zs, evenDistances);

            List<Vector3> smoothPath = new List<Vector3>();

            for (int i = 1; i < count; i++)
            {
                Vector3 targetPoint = new Vector3((float)xsOut[i], (float)ysOut[i], (float)zsOut[i]);
                smoothPath.Add(targetPoint);
            }

            return smoothPath;
        }

        private static float DistanceSquared(Vector3 point1, Vector3 point2)
        {
            float dx = point2.X - point1.X;
            float dy = point2.Y - point1.Y;
            float dz = point2.Z - point1.Z;

            return dx * dx + dy * dy + dz * dz;
        }

        public static Vector3 ProjectPointOntoPolygon(Vector3 point, List<Vector3> polygon)
        {
            Vector3 projectedPoint = Vector3.Zero;
            float minDistance = float.MaxValue;

            for (int i = 0; i < polygon.Count; i++)
            {
                int nextIndex = (i + 1) % polygon.Count;

                Vector3 v1 = polygon[i];
                Vector3 v2 = polygon[nextIndex];

                Vector3 edge = v2 - v1;
                Vector3 toPoint = point - v1;

                double distance = Vector3.Dot(toPoint, edge) / Vector3.Dot(edge, edge);

                if (distance < 0)
                    distance = 0;
                else if (distance > 1)
                    distance = 1;

                Vector3 projection = v1 + new Vector3(edge.X * distance, edge.Y * distance, edge.Z * distance);
                float distSq = DistanceSquared(point, projection);

                if (distSq < minDistance)
                {
                    minDistance = distSq;
                    projectedPoint = projection;
                }
            }

            return projectedPoint;
        }
        
        public static float GetDistance(List<Vector3> points)
        {
            float totalDistance = 0;

            if (points == null || points.Count < 2)
                return totalDistance;

            for (int i = 0; i < points.Count - 1; i++)
                totalDistance += Vector3.Distance(points[i], points[i + 1]);

            return totalDistance;
        }

        private static double[] Interpolate(double[] xOrig, double[] yOrig, double[] xInterp)
        {
            (double[] a, double[] b) = FitMatrix(xOrig, yOrig);

            double[] yInterp = new double[xInterp.Length];
            for (int i = 0; i < yInterp.Length; i++)
            {
                int j;
                for (j = 0; j < xOrig.Length - 2; j++)
                    if (xInterp[i] <= xOrig[j + 1])
                        break;

                double dx = xOrig[j + 1] - xOrig[j];
                double t = (xInterp[i] - xOrig[j]) / dx;
                double y = (1 - t) * yOrig[j] + t * yOrig[j + 1] +
                    t * (1 - t) * (a[j] * (1 - t) + b[j] * t);
                yInterp[i] = y;
            }

            return yInterp;
        }

        private static (double[] a, double[] b) FitMatrix(double[] x, double[] y)
        {
            int n = x.Length;
            double[] a = new double[n - 1];
            double[] b = new double[n - 1];
            double[] r = new double[n];
            double[] A = new double[n];
            double[] B = new double[n];
            double[] C = new double[n];

            double dx1, dx2, dy1, dy2;

            dx1 = x[1] - x[0];
            C[0] = 1.0f / dx1;
            B[0] = 2.0f * C[0];
            r[0] = 3 * (y[1] - y[0]) / (dx1 * dx1);

            for (int i = 1; i < n - 1; i++)
            {
                dx1 = x[i] - x[i - 1];
                dx2 = x[i + 1] - x[i];
                A[i] = 1.0f / dx1;
                C[i] = 1.0f / dx2;
                B[i] = 2.0f * (A[i] + C[i]);
                dy1 = y[i] - y[i - 1];
                dy2 = y[i + 1] - y[i];
                r[i] = 3 * (dy1 / (dx1 * dx1) + dy2 / (dx2 * dx2));
            }

            dx1 = x[n - 1] - x[n - 2];
            dy1 = y[n - 1] - y[n - 2];
            A[n - 1] = 1.0f / dx1;
            B[n - 1] = 2.0f * A[n - 1];
            r[n - 1] = 3 * (dy1 / (dx1 * dx1));

            double[] cPrime = new double[n];
            cPrime[0] = C[0] / B[0];
            for (int i = 1; i < n; i++)
                cPrime[i] = C[i] / (B[i] - cPrime[i - 1] * A[i]);

            double[] dPrime = new double[n];
            dPrime[0] = r[0] / B[0];
            for (int i = 1; i < n; i++)
                dPrime[i] = (r[i] - dPrime[i - 1] * A[i]) / (B[i] - cPrime[i - 1] * A[i]);

            double[] k = new double[n];
            k[n - 1] = dPrime[n - 1];
            for (int i = n - 2; i >= 0; i--)
                k[i] = dPrime[i] - cPrime[i] * k[i + 1];

            for (int i = 1; i < n; i++)
            {
                dx1 = x[i] - x[i - 1];
                dy1 = y[i] - y[i - 1];
                a[i - 1] = k[i - 1] * dx1 - dy1;
                b[i - 1] = -k[i] * dx1 + dy1;
            }

            return (a, b);
        }

        private static Vector3 ClosestPointOnLineSegment(float px, float py, float pz, float ax, float ay, float az, float bx, float by, float bz)
        {
            float apx = px - ax;
            float apy = py - ay;
            float apz = pz - az;
            float abx = bx - ax;
            float aby = by - ay;
            float abz = bz - az;
            float abMag = abx * abx + aby * aby + abz * abz; // Sqr magnitude.
            if (abMag < float.Epsilon) return new Vector3(ax, ay, az);
            // Normalize.
            abMag = (float)Math.Sqrt(abMag);
            abx /= abMag;
            aby /= abMag;
            abz /= abMag;
            float mu = abx * apx + aby * apy + abz * apz; // Dot.
            if (mu < 0) return new Vector3(ax, ay, az);
            if (mu > abMag) return new Vector3(bx, by, bz);
            return new Vector3(ax + abx * mu, ay + aby * mu, az + abz * mu);
        }

        public static Vector3 ClosestPointOnLineSegment(Vector3 p, Vector3 a, Vector3 b)
        {
            return ClosestPointOnLineSegment(p.X, p.Y, p.Z, a.X, a.Y, a.Z, b.X, b.Y, b.Z);
        }
    }
}