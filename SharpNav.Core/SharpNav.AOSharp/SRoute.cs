﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using SharpNav.Pathfinding;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AOSharp.Pathfinding
{
    public class SRoute
    {
        public string Name;
        internal List<SLink> Links;

        public SRoute(string routeName, List<SLink> links)
        {
            Name = routeName;
            Links = links;
        }
    }
}