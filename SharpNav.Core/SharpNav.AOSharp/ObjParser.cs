﻿using SharpNav.Geometry;
using System.Collections.Generic;
using System;
using System.IO;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using AOSharp.Common.GameData;
using System.Diagnostics;
using System.Threading.Tasks;
using AOSharp.Core;
using System.Text;

public static class ObjParser
{
    public static void SaveCurrentPlayfield(string folderPath, bool mergeTriangles = true)
    {
        Stopwatch sw = Stopwatch.StartNew();

        Task.Run(() =>
        {
            try
            {
                var triData = TerrainData.GetTriGeometry(Rect.Default);
                var filePath = $"{folderPath}\\{Playfield.ModelIdentity.Instance}.obj";

                if (mergeTriangles)
                    ExportAndMergeTrianglesToObj(triData, filePath);
                else
                    ExportTrianglesToObj(triData, filePath);

                Chat.WriteLine($"Total time: {sw.ElapsedMilliseconds.FormatTime()}", ChatColor.Green);
                sw.Stop();

                Chat.WriteLine($"Playfield saved at '{filePath}'", ChatColor.Green);
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.ToString());  
            }
        });
    }

    private static void ExportAndMergeTrianglesToObj(IEnumerable<Triangle3> triangles, string filePath)
    {
        Stopwatch sw = Stopwatch.StartNew();
        Dictionary<SharpNav.Geometry.Vector3, int> vertexIndices = new Dictionary<SharpNav.Geometry.Vector3, int>();
        List<SharpNav.Geometry.Vector3> vertices = new List<SharpNav.Geometry.Vector3>();
        List<int> indices = new List<int>();
        int index = 1;

        Chat.WriteLine($"Starting triangle to obj conversion.", ChatColor.LightBlue);

        foreach (var triangle in triangles)
        {
            ProcessVertex(triangle.A, vertexIndices, vertices, indices, ref index);
            ProcessVertex(triangle.B, vertexIndices, vertices, indices, ref index);
            ProcessVertex(triangle.C, vertexIndices, vertices, indices, ref index);
        }

        Chat.WriteLine($"Merged triangles. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

        try
        {
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                foreach (var vertex in vertices)
                    writer.WriteLine($"v {vertex.X} {vertex.Y} {vertex.Z}");

                Chat.WriteLine($"Wrote vertices to obj file. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

                for (int i = 0; i < indices.Count; i += 3)
                    writer.WriteLine($"f {indices[i]} {indices[i + 1]} {indices[i + 2]}");

                Chat.WriteLine($"Wrote indices to obj file. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);
            }
        }
        catch (Exception ex)
        {
            Chat.WriteLine($"Error exporting OBJ file: {ex.Message}", ChatColor.Red);
        }
    }
    public static List<Triangle3> ReadObjFile(string filePath)
    {
        List<Triangle3> triangles = new List<Triangle3>();
        List<SharpNav.Geometry.Vector3> vertices = new List<SharpNav.Geometry.Vector3>();
        List<int> indices = new List<int>();
        Stopwatch sw = Stopwatch.StartNew();

        try
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    if (line.StartsWith("v "))
                    {
                        string[] values = line.Split(' ');
                        vertices.Add(new SharpNav.Geometry.Vector3(float.Parse(values[1]), float.Parse(values[2]), float.Parse(values[3])));
                    }
                    else if (line.StartsWith("f "))
                    {
                        string[] values = line.Split(' ');
                        indices.AddRange(new int[3] { int.Parse(values[1]) - 1, int.Parse(values[2]) - 1, int.Parse(values[3]) - 1 });
                    }
                }
            }

            for (int i = 0; i < indices.Count; i += 3)
            {
                Triangle3 triangle = new Triangle3(vertices[indices[i]], vertices[indices[i + 1]], vertices[indices[i + 2]]);
                triangles.Add(triangle);
            }

            Chat.WriteLine($"Obj file loaded. {sw.ElapsedMilliseconds.FormatTime()}");
        }
        catch (Exception ex)
        {
            Chat.WriteLine($"Error reading OBJ file: {ex.Message}");
        }

        return triangles;
    }
    private static void ProcessVertex(SharpNav.Geometry.Vector3 vertex, Dictionary<SharpNav.Geometry.Vector3, int> vertexIndices, List<SharpNav.Geometry.Vector3> vertices, List<int> indices, ref int index)
    {
        if (!vertexIndices.ContainsKey(vertex))
        {
            vertexIndices[vertex] = index;
            vertices.Add(vertex);
            indices.Add(index);
            index++;
        }
        else
        {
            indices.Add(vertexIndices[vertex]);
        }
    }

    private static void ExportTrianglesToObj(IEnumerable<Triangle3> triangles, string filePath)
    {
        Stopwatch sw = Stopwatch.StartNew();

        Chat.WriteLine($"Starting triangle to obj conversion.", ChatColor.LightBlue);

        try
        {
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                foreach (var triangle in triangles)
                {
                    writer.WriteLine($"v {triangle.A.X} {triangle.A.Y} {triangle.A.Z}");
                    writer.WriteLine($"v {triangle.B.X} {triangle.B.Y} {triangle.B.Z}");
                    writer.WriteLine($"v {triangle.C.X} {triangle.C.Y} {triangle.C.Z}");
                }


                Chat.WriteLine($"Wrote vertices to obj file. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

                int index = 1;

                foreach (var _ in triangles)
                    writer.WriteLine($"f {index++} {index++} {index++}");

                Chat.WriteLine($"Wrote indices to obj file. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

            }
        }
        catch (Exception ex)
        {
            Chat.WriteLine($"Error exporting OBJ file: {ex.Message}", ChatColor.Red);
        }
    }
}