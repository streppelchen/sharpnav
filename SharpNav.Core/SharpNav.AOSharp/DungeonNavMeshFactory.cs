﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using SharpNav;
using SharpNav.Geometry;
using SharpNav.IO.Json;
using SharpNav.Pathfinding;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AVector3 = AOSharp.Common.GameData.Vector3;
using SVector3 = SharpNav.Geometry.Vector3;

namespace AOSharp.Pathfinding
{
    public class DungeonNavMeshFactory
    {
        private DirectoryInfo _roomTileDirectory;
        private NavMeshGenerationSettings _bakeSettings;
        private NavMeshSerializer _navmeshSerializer;

        public DungeonNavMeshFactory()
        {
            _roomTileDirectory = Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\RoomTiles");
            _bakeSettings = NavMeshGenerationSettings.HighDensity;
            InternalSet();
        }

        public DungeonNavMeshFactory(string roomTilePath, NavMeshGenerationSettings bakeSettings)
        {
            _roomTileDirectory = Directory.CreateDirectory(roomTilePath);
            _bakeSettings = bakeSettings;
            InternalSet();
        }

        private void InternalSet()
        {
            _navmeshSerializer = new NavMeshSerializer();
        }

        public NavMesh GenerateNavMesh()
        {
            NavMesh navMesh = new NavMesh(SVector3.Zero, 3000, 3000, Playfield.Rooms.Count, short.MaxValue);

            //int i = 1;
            //int numMissingTiles = Playfield.Rooms.Count(x => !File.Exists($"{_roomTileDirectory}\\{x.Name}.NavTile"));

            foreach (Room room in Playfield.Rooms)
            {
                //if (room.Instance != 15 && room.Instance != 30)
                //   continue;

                //if (room.Instance == 12)
                //    continue;

                //if (room.Floor != DynelManager.LocalPlayer.Room.Floor)
                //    continue;

                string roomName = !string.IsNullOrEmpty(room.Name) ? room.Name : $"{(int)Playfield.ModelId}_{room.Instance}";
                string fileName = $"{_roomTileDirectory.FullName}\\{roomName}.NavTile";
                RoomTile tile;

                if (File.Exists(fileName))
                {
                    tile = _navmeshSerializer.DeserializeRoomTile(fileName);
                }
                else
                {
                    Chat.WriteLine($"Baking {roomName}. {room.Instance}");
                    var triMesh = TerrainData.GetTriGeometry(room);

                    triMesh = triMesh.Select(x => new Triangle3
                    {
                        A = (Quaternion.AngleAxis(360 - room.Rotation, AVector3.Up) * (x.A.ToVector3() - room.Position)).ToSharpNav(),
                        B = (Quaternion.AngleAxis(360 - room.Rotation, AVector3.Up) * (x.B.ToVector3() - room.Position)).ToSharpNav(),
                        C = (Quaternion.AngleAxis(360 - room.Rotation, AVector3.Up) * (x.C.ToVector3() - room.Position)).ToSharpNav(),
                    }).ToList();

                    SNavMeshGenerator.GeneratePolyMesh(triMesh, _bakeSettings, out PolyMesh polyMesh, out PolyMeshDetail detailMesh);

                    tile = new RoomTile
                    {
                        PolyMesh = polyMesh,
                        DetailMesh = detailMesh,
                        Settings = _bakeSettings
                    };

                    tile.FilterPolyMesh(room);

                    _navmeshSerializer.Serialize(fileName, polyMesh, detailMesh, _bakeSettings);
                }

                tile.SnapToRoom(room);
                var polyId = navMesh.AddTile(tile.ToBuilder(room));
            }

            foreach (NavTile tile in navMesh.Tiles)
                navMesh.InitTile(tile);

            return navMesh;
        }

        public async Task<NavMesh> GenerateNavMeshAsync()
        {
            return await Task.Run(() =>
            {
                return GenerateNavMesh();
            });
        }
    }

    public class RoomTile
    {
        public PolyMesh PolyMesh;
        public PolyMeshDetail DetailMesh;
        public NavMeshGenerationSettings Settings;

        public NavMeshBuilder ToBuilder(Room room)
        {
            var builder = new NavMeshBuilder(PolyMesh, DetailMesh, GetDoorConnections(room), Settings);
            builder.Header.Layer = room.Instance;
            return builder;
        }

        public void FilterPolyMesh(Room room)
        {
            room.GetDoorPosRot(0, out AVector3 doorPos, out _);

            var mainPolies = GetMainPolies((Quaternion.AngleAxis(360 - room.Rotation, AVector3.Up) * (doorPos - room.Position)));
            var badPolys = PolyMesh.Polys.Where(x => !mainPolies.Contains(Array.IndexOf(PolyMesh.Polys, x)));

            foreach (var badPoly in badPolys)
                badPoly.Area = Area.Null;
        }

        public HashSet<int> GetMainPolies(AVector3 doorPos)
        {
            PolyVertex closestVertex = PolyMesh.Verts.OrderBy(x => AVector3.Distance(doorPos, x.ToVector3(PolyMesh.Bounds, PolyMesh.CellSize, PolyMesh.CellHeight))).First();
            int closestVertexIdx = Array.IndexOf(PolyMesh.Verts, closestVertex);
            PolyMesh.Polygon closestPolygon = PolyMesh.Polys.First(x => x.Vertices.Contains(closestVertexIdx));
            var mainPolys = GetAllConnections(closestPolygon);

            return mainPolys;
        }

        public HashSet<int> GetAllConnections(PolyMesh.Polygon polygon)
        {
            HashSet<int> polys = new HashSet<int>();
            Stack<int> polyStack = new Stack<int>();
            polyStack.Push(Array.IndexOf(PolyMesh.Polys, polygon));

            while (polyStack.Any())
            {
                var poly = polyStack.Pop();
                polys.Add(poly);

                foreach (var nei in PolyMesh.Polys[poly].NeighborEdges)
                {
                    if (nei == -1 || polys.Contains(nei))
                        continue;

                    polyStack.Push(nei);
                }
            }

            return polys;
        }


        private OffMeshConnection[] GetDoorConnections(Room room)
        {
            List<OffMeshConnection> offMeshConnections = new List<OffMeshConnection>();

            for (int i = 0; i < room.NumDoors; i++)
            {
                if (room.GetDoorConnectZone(i) == room.Instance)
                    continue;

                room.GetDoorPosRot(i, out var pos, out var rot);

                offMeshConnections.Add(new OffMeshConnection
                {
                    Pos0 = (pos + rot.Forward).ToSharpNav(),
                    Pos1 = (pos - rot.Forward).ToSharpNav(),
                    Flags = OffMeshConnectionFlags.None,
                    Radius = 0.25f
                });
            }

            return offMeshConnections.ToArray();
        }

        public void SnapToRoom(Room room)
        {
            //Chat.WriteLine($"Old Bounds: {PolyMesh.Bounds}");
            PolyMesh.Verts = PolyMesh.Verts.Select(x => TransformPolyVertex(x, room)).ToArray();
            DetailMesh.Verts = DetailMesh.Verts.Select(x => (Quaternion.AngleAxis(room.Rotation, AVector3.Up) * x.ToVector3() + room.Position).ToSharpNav()).ToArray();

            PolyMesh.Bounds = TransformBounds(PolyMesh.Bounds, room);
            //Chat.WriteLine($"New Bounds: {PolyMesh.Bounds}");
            //Chat.WriteLine($"New Bounds: Min: {PolyMesh.Bounds.Min.ToVector3() - room.Position} Max: {PolyMesh.Bounds.Max.ToVector3() - room.Position}");
        }

        private PolyVertex TransformPolyVertex(PolyVertex v, Room room)
        {
            AVector3 realPos = PolyMesh.Bounds.Min.ToVector3() + new AVector3(v.X * Settings.CellSize, v.Y * Settings.CellHeight, v.Z * Settings.CellSize);
            BBox3 rotatedMeshBounds = TransformBounds(PolyMesh.Bounds, room);
            AVector3 rotated = Quaternion.AngleAxis(room.Rotation, AVector3.Up) * realPos - (rotatedMeshBounds.Min.ToVector3() - room.Position);
            AVector3 transformedVertex = new AVector3(rotated.X / Settings.CellSize, rotated.Y / Settings.CellHeight, rotated.Z / Settings.CellSize);

            //Chat.WriteLine($"V: {v}, RealPos: {realPos}, Rotated: {rotated}, TransVert: {transformedVertex}, RotatedMeshBounds: {rotatedMeshBounds}, Reversed: {rotatedMeshBounds.Min.ToVector3() - PolyMesh.Bounds.Min.ToVector3() + new AVector3(transformedVertex.X * Settings.CellSize, transformedVertex.Y * Settings.CellHeight, transformedVertex.Z * Settings.CellSize)}");

            return new PolyVertex((int)transformedVertex.X, (int)transformedVertex.Y, (int)transformedVertex.Z);
        }

        private BBox3 TransformBounds(BBox3 bounds, Room room)
        {
            SVector3 outMin = bounds.Min;
            SVector3 outMax = bounds.Max;

            if (room.Rotation > 0)
            {
                SVector3 min = (Quaternion.AngleAxis(room.Rotation, AVector3.Up) * PolyMesh.Bounds.Min.ToVector3()).ToSharpNav();
                SVector3 max = (Quaternion.AngleAxis(room.Rotation, AVector3.Up) * PolyMesh.Bounds.Max.ToVector3()).ToSharpNav();

                switch (room.Rotation)
                {
                    case 90:
                        outMin = new SVector3(min.X, bounds.Min.Y, max.Z);
                        outMax = new SVector3(max.X, bounds.Max.Y, min.Z);
                        break;
                    case 180:
                        outMin = new SVector3(max.X, bounds.Min.Y, max.Z);
                        outMax = new SVector3(min.X, bounds.Max.Y, min.Z);
                        break;
                    case 270:
                        outMin = new SVector3(max.X, bounds.Min.Y, min.Z);
                        outMax = new SVector3(min.X, bounds.Max.Y, max.Z);
                        break;
                }
            }

            return new BBox3
            {
                Min = outMin + room.Position.ToSharpNav(),
                Max = outMax + room.Position.ToSharpNav()
            };
        }
    }
}
