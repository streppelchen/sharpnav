﻿using AOSharp.Common.GameData;

namespace AOSharp.Pathfinding
{
    /// <summary>
    /// Playfield nav mesh linking. Used for linking across different playfields.
    /// </summary>
    public class SPlayfieldLink : SNavLink
    {
        public bool UseOnArrival;

        public SPlayfieldLink(Vector3 position, string name, LinkType linkType, int destId, bool useOnArrival = true) : base(position, name, linkType, destId)
        {
            UseOnArrival = useOnArrival;
        }
    }
}
