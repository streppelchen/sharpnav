﻿using AOSharp.Core;
using AOSharp.Common.GameData;
using System.Linq;
using System.Collections.Generic;
using System;
using Vector3 = AOSharp.Common.GameData.Vector3;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using AOSharp.Core.UI;
using SharpNav;
using AOSharp.Common.Unmanaged.Imports;
using SmokeLounge.AOtomation.Messaging.GameData;
using System.Collections.Concurrent;
using AOSharp.Core.Misc;
using SharpNav.Pathfinding;
using static SharpNav.NavMeshQuery;
using SmokeLounge.AOtomation.Messaging.Messages;

namespace AOSharp.Pathfinding
{
    public class SMovementController
    {
        public static SNavAgent NavAgent;
        public static Action<Vector3> DestinationReached;
        public static Action<Vector3> OnRubberband;
        public static Action<SNavLink> LinkDestinationReached;
        public static Action<Vector3, Vector3> Stuck;
        public static Action<AgentNavMeshState> AgentStateChange;
        public static NavMeshGenerationSettings AutoBakeSettings;

        internal static Vector3 Extents => _settings.PathSettings.Extents;

        private static SMovementControllerSettings _settings;
        private static AutoResetInterval _rotUpdate;
        private static AutoResetInterval _movementUpdate;
        private static AutoResetInterval _unstuckCheck;
        private static float _lastDist = 0f;
        private static float _linkDistThreshold = 2;
        private static ConcurrentQueue<MovementAction> _movementActionQueue = new ConcurrentQueue<MovementAction>();
        private static Vector3 _unstuckDir = new Vector3(0, 1.667, 1);
        private static Vector3 _waypoint;
        private static bool _loaded = false;
        private static Func<int, bool, bool> _shouldAutoBake;
        private static Func<int, bool, bool> _shouldAutoLoad;
        private static string _navmeshLoadPath;
        private static string _navmeshBakePath;
        private static bool _loadAfterBaking;
        private static AgentNavMeshState _agentState;
        private static Dictionary<int, List<SPlayfieldLink>> _playfieldLinks;
        private static StuckLogicDelegate _onStuckLogic = DefaultStuckLogic;
        public delegate void StuckLogicDelegate();

        public static bool IsLoaded()
        {
            return _loaded;
        }

        public static void Set(SMovementControllerSettings settings)
        {
            InternalSet(settings);
        }

        public static void AutoBakeNavmeshes(string folderPath, bool loadAfterBaking = true, Func<int, bool, bool> shouldAutoBake = null, NavMeshGenerationSettings bakeSettings = null)
        {
            _navmeshBakePath = folderPath;
            _loadAfterBaking = loadAfterBaking;
            AutoBakeSettings = bakeSettings == null ? NavMeshGenerationSettings.MediumDensity : bakeSettings;
            _shouldAutoBake = shouldAutoBake == null ? (int x, bool y) => true : shouldAutoBake;
            AutoBakeNavmesh();
        }

        public static void AutoLoadNavmeshes(string folderPath, Func<int, bool, bool> shouldAutoLoad = null)
        {
            _navmeshLoadPath = folderPath;
            _shouldAutoLoad = shouldAutoLoad == null ? (int x, bool y) => true : shouldAutoLoad;
            AutoLoadNavmesh();
        }

        private static void AutoBakeNavmesh()
        {
            if (_navmeshBakePath == null)
                return;

            var path = $"{_navmeshBakePath}\\{Playfield.ModelIdentity.Instance}.nav";

            if (System.IO.File.Exists(path) || !_shouldAutoBake(Playfield.ModelIdentity.Instance, Playfield.IsDungeon))
                return;

            SNavMeshGenerator.GenerateAsync(AutoBakeSettings).ContinueWith(autoBake =>
            {
                if (autoBake.Result == null)
                    return;

                if (_loadAfterBaking)
                    LoadNavmesh(autoBake.Result);

                SNavMeshSerializer.SaveToFile(autoBake.Result, path);
            });
        }

        private static void AutoLoadNavmesh()
        {
            if (_navmeshLoadPath == null)
                return;

            var path = $"{_navmeshLoadPath}\\{Playfield.ModelIdentity.Instance}.nav";

            if (!System.IO.File.Exists(path) || !_shouldAutoLoad(Playfield.ModelIdentity.Instance, Playfield.IsDungeon))
                return;

            if (!SNavMeshSerializer.LoadFromFile(path, out NavMesh navMesh))
            {
                Chat.WriteLine("Tried to load navmesh on zone but failed (could not be deserialized)");
                return;
            }

            if (InstanceIsNull())
                return;

            LoadNavmesh(navMesh);
        }

        public static void Set()
        {
            InternalSet(new SMovementControllerSettings());
        }

        public static void RegisterPfLink(int sourceZoneId, SPlayfieldLink zoneLink)
        {
            if (!_playfieldLinks.ContainsKey(sourceZoneId))
                _playfieldLinks.Add(sourceZoneId, new List<SPlayfieldLink>());

            _playfieldLinks[sourceZoneId].Add(zoneLink);
        }

        public static void ToggleNavMeshDraw()
        {
            if (InstanceIsNull())
                return;

            _settings.NavMeshSettings.DrawNavMesh = !_settings.NavMeshSettings.DrawNavMesh;
        }

        public static void TogglePathDraw()
        {
            if (InstanceIsNull())
                return;

            _settings.PathSettings.DrawPath = !_settings.PathSettings.DrawPath;
        }

        public static void Follow(Identity identity)
        {
            if (InstanceIsNull())
                return;

            NavAgent.FollowTarget(identity);
        }

        public static void Halt()
        {
            if (InstanceIsNull())
                return;

            NavAgent.Stop();
        }

        public static bool SetNavDestination(int zoneId, bool ignoreIfAlreadyNavigating = false)
        {
            return SetNavDestination(zoneId, PathingType.Shortest, ignoreIfAlreadyNavigating);
        }

        public static bool SetNavDestination(int zoneId, PathingType pathingType, bool ignoreIfAlreadyNavigating = false)
        {
            if (!_playfieldLinks.TryGetValue(Playfield.ModelIdentity.Instance, out List<SPlayfieldLink> hardLinks))
            {
                Chat.WriteLine($"No valid links found for zone '{Playfield.ModelIdentity.Instance}'");
                return false;
            }

            hardLinks = hardLinks.Where(x => x.DestId == zoneId).ToList();

            if (hardLinks.Count() == 0)
            {
                Chat.WriteLine($"No valid destination links found for zone '{Playfield.ModelIdentity.Instance}' -> '{zoneId}'");
                return false;
            }

            Vector3 finalDestination = Vector3.Zero;
            float distance = float.MaxValue;

            foreach (var link in hardLinks)
            {
                var newPath = GenerateNavPath(link.Position);
                var newPathDistance = MathExtras.GetDistance(newPath);

                if (newPathDistance < distance)
                {
                    finalDestination = link.Position;
                    distance = newPathDistance;
                }
            }

            if (finalDestination == Vector3.Zero)
            {
                Chat.WriteLine("No valid path found");
                return false;
            }

            return SetNavDestination(finalDestination, pathingType, ignoreIfAlreadyNavigating);
        }

        public static bool SetNavDestination(Vector3 destination, bool ignoreIfAlreadyNavigating = false)
        {
            return SetNavDestination(destination, PathingType.Shortest, ignoreIfAlreadyNavigating);
        }

        public static bool SetNavDestination(Vector3 destination, PathingType pathType, bool ignoreIfAlreadyNavigating = false)
        {
            if (InstanceIsNull())
                return false;

            if (NavAgent.IsNavigating && ignoreIfAlreadyNavigating)
                return false;

            switch (pathType)
            {
                case PathingType.Interpolated:
                    NavAgent.SetSmoothNavDestination(destination, 1.25f);
                    break;
                case PathingType.Shortest:
                    NavAgent.SetNavDestination(destination);
                    break;
            }

            return true;
        }

        internal static List<Vector3> GenerateNavPath(Vector3 destination)
        {
            var path = new List<Vector3>();

            if (InstanceIsNull())
                return path;

            return NavAgent.GenerateNavPath(DynelManager.LocalPlayer.Position, destination);
        }

        public static List<Vector3> GenerateNavPath(Vector3 start,Vector3 destination)
        {
            var path = new List<Vector3>();

            if (InstanceIsNull())
                return path;

            return NavAgent.GenerateNavPath(start, destination);
        }

        public static double NavmeshDistanceBetweenPoints(Vector3 start, Vector3 destination)
        {
            //should always be at least 2 points, start and destination.
            try
            {
                var path = GenerateNavPath(start, destination);
                double distance = start.Distance2DFrom(path[0]);

                for (int i = 1; i < path.Count; i++)
                {
                    distance += path[i - 1].Distance2DFrom(path[i]);
                }
                return distance;
            }
            catch(Exception ex)
            {
                Chat.WriteLine(ex.ToString());
                Chat.WriteLine(ex.StackTrace.ToString());
                return 0.0;
            }
        }

        public static Vector3 GetRandomPoint()
        {
            if (InstanceIsNull())
                return Vector3.Zero;

            return NavAgent.GetRandomPoint();
        }

        public static Vector3 GetClosestNavPoint(Vector3 position)
        {
            if (InstanceIsNull())
                return Vector3.Zero;

            if (NavAgent.FindNearestNavPoint(position, Extents, out NavPoint point))
                return point.Position.ToVector3();

            return Vector3.Zero;
        }

        public static bool GetClosestNavPoint(Vector3 extents, out Vector3 position)
        {
            position = Vector3.Zero;

            if (InstanceIsNull())
                return false;

            if (!NavAgent.FindNearestNavPoint(DynelManager.LocalPlayer.Position, extents, out NavPoint point))
                return false;

            position = point.Position.ToVector3();

            return true;
        }

        public static Vector3 GetClosestNavPoint()
        {
            return GetClosestNavPoint(DynelManager.LocalPlayer.Position);
        }

        public static void AppendNavDestination(Vector3 destination)
        {
            AppendNavDestination(DynelManager.LocalPlayer.Position, destination);
        }

        public static void AppendNavDestination(Vector3 startPos, Vector3 destination)
        {
            if (InstanceIsNull())
                return;

            NavAgent.AppendNavDestination(startPos, destination);
        }

        public static bool IsNavigating()
        {
            if (InstanceIsNull())
                return false;

            return NavAgent.IsNavigating;
        }

        public static void LoadNavmesh(NavMesh navMesh)
        {
            if (InstanceIsNull())
                return;

            NavAgent.SetNavmesh(navMesh);
        }

        public static void UnloadNavmesh()
        {
            if (InstanceIsNull())
                return;

            NavAgent?.DeleteNavmesh();
        }

        public static void SetMovement(MovementAction action)
        {
            _movementActionQueue.Enqueue(action);
        }

        public static bool SetPath(SPath sPath, bool startAtClosestPoint = false)
        {
            if (InstanceIsNull())
                return false;

            if (!GetPoints(sPath, startAtClosestPoint, sPath.IsReversed, out var points))
                return false;

            return SetWaypoints(points);
        }

        public static bool SetDestination(Vector3 destination, bool ignoreIfAlreadyNavigating = false)
        {
            if (NavAgent.IsNavigating && ignoreIfAlreadyNavigating)
                return false;

            SetWaypoints(new List<Vector3> { destination });

            return true;
        }

        public static bool AppendPath(SPath sPath, bool startAtClosestPoint, bool reverseDirection)
        {
            if (!GetPoints(sPath, startAtClosestPoint, reverseDirection, out var points))
                return false;

            return AppendWaypoints(points);
        }

        private static bool GetPoints(SPath sPath, bool startAtClosestPoint, bool reverseDirection, out List<Vector3> points) 
        {
            points = new List<Vector3>();

            if (sPath == null || sPath.Waypoints == null || sPath.Waypoints.Count == 0)
                return false;

            sPath.IsLocked = true;

            points = sPath.GetWaypoints(out _);

            if (reverseDirection)
                points.Reverse();

            if (startAtClosestPoint)
            {
                var closestPoint = points.OrderBy(x => Vector3.Distance(DynelManager.LocalPlayer.Position, x)).FirstOrDefault();
                var index = points.IndexOf(closestPoint);
                points.RemoveRange(0, index);
            }

            return true;
        }

        internal static bool SetWaypoints(List<Vector3> waypoints)
        {
            if (waypoints.Count() == 0 || waypoints == null)
                return false;

            NavAgent.Path.Clear();
            _lastDist = 0;
            return AppendWaypoints(waypoints);
        }

        internal static bool AppendWaypoints(List<Vector3> waypoints)
        {
            if (waypoints == null || waypoints.Count() == 0)
                return false;

            Path path = new Path(waypoints, _settings.PathSettings.PathRadius, _settings.PathSettings.PathRadius);

            if (path.Waypoints.Count == 0)
                return false;

            NavAgent.Path.Enqueue(path);
            return true;
        }

        private static void OnStuck(Vector3 destination)
        {
            Stuck?.Invoke(DynelManager.LocalPlayer.Position, destination);
            _onStuckLogic?.Invoke();
        }

        private static void DefaultStuckLogic()
        {
            DynelManager.LocalPlayer.Position += DynelManager.LocalPlayer.Rotation * _unstuckDir * 0.5f;
        }

        public static void SetStuckLogic(StuckLogicDelegate newLogic)
        {
            _onStuckLogic = newLogic;
        }

        private static bool InstanceIsNull()
        {
            if (NavAgent != null)
                return false;

            Chat.WriteLine("SMovementController not set. (SMovementController.Set() not executed?)", ChatColor.Red);

            return true;
        }

        private static void Update(object sender, float deltaTime)
        {
            try
            {
                if (NavAgent == null)
                    return;

                if (NavAgent.HasPathfinder && _settings.NavMeshSettings.DrawNavMesh)
                    DebugDrawUpdate();

                while (_movementActionQueue.TryDequeue(out MovementAction action))
                    ChangeMovement(action);

                if (NavAgent.IsNavigating)
                    PathUpdate(deltaTime);
            }
            catch (Exception e)
            {
                Chat.WriteLine($"This shouldn't happen pls report (SMovementController): {e.Message}");
            }
        }

        private static void PathUpdate(float deltaTime)
        {
            Path currentPath = NavAgent.CurrentPath;

            if (!NavAgent.IsMoving)
            {
                _unstuckCheck.Reset();
                SetMovement(MovementAction.ForwardStart);
            }

            if (!currentPath.Initialized)
                currentPath.OnPathStart();

            if (!currentPath.GetNextWaypoint(out Vector3 waypoint))
            {
                Path currPath = NavAgent.Path.Dequeue();
                currPath.OnPathFinished();
                _lastDist = 0;

                if (!NavAgent.IsNavigating && currPath.StopAtDest)
                {
                    if (_playfieldLinks.TryGetValue(Playfield.ModelIdentity.Instance, out List<SPlayfieldLink> links))
                        OnSPlayfieldLinkReached(links);

                    DestinationReached?.Invoke(_waypoint);
                    SetMovement(MovementAction.ForwardStop);
                }

                return;
            }
            else
            {
                _waypoint = waypoint;
            }

            if (_unstuckCheck.Elapsed && NavAgent.IsMoving)
                OnStuckUpdate(currentPath);

            if (_rotUpdate.Elapsed)
                NavAgent.LerpTowards(waypoint, _settings.PathSettings.MinRotSpeed, _settings.PathSettings.MaxRotSpeed, deltaTime);

            if (_movementUpdate.Elapsed)
                SetMovement(MovementAction.Update);

            if (_settings.PathSettings.DrawPath && NavAgent.IsNavigating)
                currentPath.Draw();
        }

        private static void OnStuckUpdate(Path currentPath)
        {
            float currentDist = NavAgent.CurrentPath.Length();

            if (_lastDist != 0 && currentDist >= _lastDist)
            {
                if (currentPath.PeekLastWaypoint(out Vector3 peekPoint))
                    OnStuck(peekPoint);
            }

            _lastDist = currentDist;
        }

        private static void OnSPlayfieldLinkReached(List<SPlayfieldLink> links)
        {
            SPlayfieldLink link = links.FirstOrDefault(x => Vector3.Distance(x.Position, _waypoint) < _linkDistThreshold);

            if (link == null)
                return;

            switch (link.LinkType)
            {
                case LinkType.Portal:
                    if (!link.UseOnArrival)
                        break;

                    DynelManager.AllDynels
                         .Where(x => x.Name == link.Name)?
                         .OrderBy(x => x.DistanceFrom(DynelManager.LocalPlayer))
                         .FirstOrDefault()?.Use();
                    break;
                case LinkType.Elevator:
                    break;
            }

            LinkDestinationReached?.Invoke(link);
        }

        private static void DebugDrawUpdate()
        {
            Vector3 meshColor = DebuggingColor.Green;
            Vector3 cylinderColor = DebuggingColor.Red;

            if (NavAgent.FindPolys(_settings.NavMeshSettings.DrawDistance, out Poly[] polies) == AgentNavMeshState.InPolygon)
            {
                for (int i = 0; i < polies.Length; i++)
                {
                    if (polies[i].Area != Area.Null)
                        polies[i].Draw(meshColor);
                }
            }

            AgentNavMeshState agentState = NavAgent.IsOnNavPoly(_settings.PathSettings.PathRadius, out Poly poly);

            if (_agentState != agentState)
            {
                _agentState = agentState;
                AgentStateChange?.Invoke(_agentState);
            }

            switch (agentState)
            {
                case AgentNavMeshState.InPolygon:
                    poly.Draw(DebuggingColor.LightBlue, Vector3.Up * 0.01f);
                    cylinderColor = DebuggingColor.Green;
                    break;
                case AgentNavMeshState.OutPolygon:
                    cylinderColor = new Vector3(1f, 0.5f, 0f);
                    break;
                case AgentNavMeshState.OutNavMesh:
                    cylinderColor = DebuggingColor.Red;
                    break;
            }

            Vector3 rayOrigin = DynelManager.LocalPlayer.Position;
            Vector3 rayTarget = DynelManager.LocalPlayer.Position;
            rayTarget.Y = 0;

            if (!Playfield.Raycast(rayOrigin, rayTarget, out Vector3 hitPos, out _))
                hitPos = rayOrigin;

            SDebug.DrawCylinder(hitPos, _settings.PathSettings.PathRadius, (float)DynelManager.LocalPlayer.GetStat(Stat.Scale) / 100, cylinderColor);
        }

        private static void InternalSet(SMovementControllerSettings settings)
        {
            NavAgent = new SNavAgent();
            _playfieldLinks = new Dictionary<int, List<SPlayfieldLink>>();
            _settings = settings;
            _rotUpdate = new AutoResetInterval(_settings.PathSettings.RotUpdate);
            _movementUpdate = new AutoResetInterval(_settings.PathSettings.MovementUpdate);
            _unstuckCheck = new AutoResetInterval(_settings.PathSettings.UnstuckUpdate);
           
            Chat.WriteLine("SMovementController was set.", ChatColor.Green);

            if (!_loaded)
            {
                Game.OnUpdate += Update;
                Game.PlayfieldInit += OnPlayfieldInit;
                Game.TeleportStarted += OnTeleportStarted;
                Network.N3MessageReceived += OnSetPosReceived;
                SPathManager.Set();
                _loaded = true;
            }
        }

        private static void OnSetPosReceived(object sender, N3Message e)
        {
            if (!(e is SetPosMessage setPosMsg))
                return;

            if (setPosMsg.Identity != DynelManager.LocalPlayer.Identity)
                return;


            OnRubberband?.Invoke(setPosMsg.Position);
        }

        private static void OnTeleportStarted(object sender, EventArgs e)
        {
            if (NavAgent.IsNavigating)
                Halt();
        }

        private static void OnPlayfieldInit(object sender, uint e)
        {
            if (NavAgent.TryGetNavmeshPfId(out int pfId) && pfId == Playfield.ModelIdentity.Instance)
                return;

            NavAgent.ResetPathfinder();
            AutoLoadNavmesh();
            AutoBakeNavmesh();
        }

        //Must be called from game loop!
        private static void ChangeMovement(MovementAction action)
        {
            if (action == MovementAction.LeaveSit)
            {
                Network.Send(new CharacterActionMessage()
                {
                    Action = CharacterActionType.StandUp
                });
            }
            else
            {
                IntPtr pEngine = N3Engine_t.GetInstance();

                if (pEngine == IntPtr.Zero)
                    return;

                N3EngineClientAnarchy_t.MovementChanged(pEngine, action, 0, 0, true);
            }
        }
    }
}

public enum PathingType
{
    Shortest,
    Interpolated
}
